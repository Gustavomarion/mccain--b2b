<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<div class='col-xs-4 col-sm-3 text-left'>
				<a href='http://mccainfoodservice.com.br'><img class='logo' src='img/logo.png'></a>
				<div class='hidden-xs selo'>
					<a href='./'><img src='img/selo_topo.png'></a>
				</div>
			</div>
			<div class='col-md-7 hidden-xs'></div>
			<div class='col-md-2 hidden-xs hidden-sm text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div> 
		</div>
		
		<div class='content'>
			<div class='sidebar col-md-3'>
				<center><img src='img/secao_fotos.png' class='agenteFaz text-center'></center>
				
				<p>Fotos de produtos prontas para voc� imprimir e usar em seu estabelecimento. Afinal, consumidor tamb�m come com os olhos.</p>

			</div>
			
			<div class='col-md-9 homeMainArea' >
				
				<div class='col-md-4 menuItem '>
					<div class='boxMenuItem col-md-12 fotoArea'>
						<img src='img/cozinha.png' class="display">

						<div class='col-md-12 downFoto'>
							<img src='img/download.png'> Baixar Foto
						</div>
					</div>
				</div>
				
				<div class='col-md-4 menuItem '>
					<div class='boxMenuItem col-md-12 fotoArea'>
						<img src='img/cozinha.png' class="display">

						<div class='col-md-12 downFoto'>
							<img src='img/download.png'> Baixar Foto
						</div>
					</div>
				</div>
				
				<div class='col-md-4 menuItem '>
					<div class='boxMenuItem col-md-12 fotoArea'>
						<img src='img/cozinha.png' class="display">

						<div class='col-md-12 downFoto'>
							<img src='img/download.png'> Baixar Foto
						</div>
					</div>
				</div>
				
				<div class='col-md-4 menuItem '>
					<div class='boxMenuItem col-md-12 fotoArea'>
						<img src='img/cozinha.png' class="display">

						<div class='col-md-12 downFoto'>
							<img src='img/download.png'> Baixar Foto
						</div>
					</div>
				</div>
				
				<div class='col-md-4 menuItem '>
					<div class='boxMenuItem col-md-12 fotoArea'>
						<img src='img/cozinha.png' class="display">

						<div class='col-md-12 downFoto'>
							<img src='img/download.png'> Baixar Foto
						</div>
					</div>
				</div>
				
				<div class='col-md-4 menuItem '>
					<div class='boxMenuItem col-md-12 fotoArea'>
						<img src='img/cozinha.png' class="display">

						<div class='col-md-12 downFoto'>
							<img src='img/download.png'> Baixar Foto
						</div>
					</div>
				</div>
				
				
				
			</div>
			<br clear='all'>
		</div>
		
		<div class='footer'>

			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Pol�tica de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
	<script>
		$(document).ready(function(){
			$(".btnBusca").click(function(){
				
				if($.trim($("#busca").val()) != "")
					location.href="resultado-busca.php?q="+$.trim($("#busca").val());
			})
			
			$('#busca').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBusca').trigger('click');
				return false;
			  }
			});
		})
	</script>
  </body>
</html>
