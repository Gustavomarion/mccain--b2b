<?php

		$dadosFull 		= json_decode(file_get_contents("json/dicas.json"),true);
		
		
		
		$dados = $dadosFull[$_GET[dica]];
		
		unset($dadosFull[$_GET[dica]]);
		$arrayKeys = @array_rand($dadosFull,3);
		
		
		foreach($arrayKeys as $key)
		{
			$newArray[$key] = $dadosFull[$key];
		}
		

		

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Cozinha McCain - <?php echo ($dados[title]);?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	

  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<div class='col-xs-4 col-sm-3 text-left'>
				<a href='http://mccainfoodservice.com.br'><img class='logo' src='img/logo.png'></a>
				<div class='hidden-xs selo'>
					<a href='./'><img src='img/selo_topo.png'></a>
				</div>
			</div>
			<div class='col-xs-8 col-sm-6 text-left mnDown'>
				<a href='./'>
					<div class='backHome'>
						<img src='img/menu_name.png'>
					</div>
				</a>
				<a href='dicas.php'>
					<div class='bread'>
						<img src='img/menu_dicas.png'>
					</div>
				</a>
			</div>
			<div class='col-md-1 hidden-xs hidden-sm'></div>
			<div class='hidden-xs col-sm-3 col-md-2 text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div> 
			
		</div>		
		<div class='content'>
			<div class='sidebar col-sm-3'>
				<div class="col-xs-6 col-sm-12 col-xs-offset-3 col-sm-offset-0 text-center">
					<img src='img/secao_dicas.png' class='agenteFaz text-center'>
				</div>
				<div class="col-xs-12">
				<p>A McCain investigou a realidade de cada segmento para ser assertiva na hora de ajudar todos os tipos de negócio. Veja aqui algumas dicas preciosas.</p>

				
				</div>
				<div class='col-xs-8 col-sm-12 col-xs-offset-2 col-sm-offset-0 text-right sidebarBusca'>
					<input type='text' placeholder="Digite sua busca"><button><i class='fa fa-search'></i></button>
				</div>

			</div>
			
			<div class='col-sm-6 itemAberto' >
				
				<div class='col-sm-12 item'>
					<h2><?php echo ($dados[title]);?></h2>
					<div class='dica-content'>
						<?php echo ($dados[content]);?>
					</div>
				
					<br clear='all'><br>
					
				</div>
				
				
			</div>
			
			
			<div class='col-sm-3 sidebarRight' >
				
				<!--
				<div class='item'>
					<br><br><br><br>
					BANNER
					<br><br><br><br>
				</div>
				-->
				
				<div class='item'>
					<h2>Outras dicas</h2>
					<?php
						$cont = 0;
						foreach($newArray as $key => $row)
						{
					?>
					<div class='item-list'>
						<a href='dica-detalhe.php?dica=<?php echo $key;?>'>
							<div class='col-md-4'>
								<div class="thumbHeightDef">
									<img src='<?php echo ($row[featured_image][guid]);?>'>
								</div>
							</div>
							<div class='col-md-8 bd-top'><?php echo ($row[title]);?></div>
						</a>
					</div>
					<br clear='all'>
					<?php 
					
					$cont++;
					if($cont > 5) break;
					} ?>
					
					<br clear='all'>
					
				</div>
			</div>
			
			
			
			<br clear='all'>
		</div>
		
		<div class='footer'>

			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Política de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
	<script>
		$(document).ready(function(){
			$(".btnBusca").click(function(){
				
				if($.trim($("#busca").val()) != "")
					location.href="resultado-busca.php?q="+$.trim($("#busca").val());
			})
			
			$('#busca').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBusca').trigger('click');
				return false;
			  }
			});
		})
	</script>
  </body>
</html>
