<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Calculadora McCain - A gente faz mais pelo seu negócio</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	

  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<div class='col-xs-4 col-sm-3 text-left'>
				<a href='http://mccainfoodservice.com.br'><img class='logo' src='img/logo.png'></a>
				<div class='hidden-xs selo'>
					<a href='./'><img src='img/selo_topo.png'></a>
				</div>
			</div>			
			<div class='col-md-3 col-sm-6 col-xs-8  text-left mnDown'>
				<a href='./'>
					<div class='backHome'>
						<img src='img/menu_name.png'>
					</div>
				</a>
			</div>			
			<div class='col-sm-4 hidden-xs hidden-sm'></div>
			<div class='hidden-xs col-sm-3 col-md-2 text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div> 
		</div>
		
		<div class='content'>
			<div class='sidebar col-sm-3'>
				<div class="col-xs-6 col-sm-12 col-xs-offset-3 col-sm-offset-0 text-center">
				<img src='img/secao_calc.png' class='agenteFaz text-center'>
				</div>
				<div class="col-xs-12">
				<p>Com essa ferramenta você não trabalha no escuro. Descubra já a quantidade

suas porções e a rentabilidade que você pode ter com elas.</p>
				</div>
				
				
			</div>
			
			<div class='col-sm-9 col-md-6 itemAberto' >
				
				<div class='col-md-12 item contato calculadora'>
					
					<div class='col-xs-12'>
						<label>Qual é o perfil do seu estabelecimento?</label>
						<select name='perfil'>
							<option value=''> Selecione </option>
							<option value=''> Hotel </option>
							<option value=''> Bar </option>
							<option value=''> Buffet Infantil </option>
							<option value=''> Churrascaria </option>
							<option value=''> Estilo Americano </option>
							<option value=''> Hamburgueria </option>
							<option value=''> Hotel </option>
							<option value=''> Lanchonete </option>
							<option value=''> Lojas de Conveniência </option>
							<option value=''> Pizzaria </option>
							<option value=''> Quiosque de Praia </option>
							<option value=''> Restaurante por Quilo </option>
							<option value=''> Restaurante a la Carte </option>
							<option value=''> Rotisserias </option>
						</select>
					</div>
					
					<div class='col-xs-12'>
						<label>Como você serve batata frita no seu estabelecimento?</label>
						<select name='como_serve'>
							<option value=''> Selecione </option>
							<option value='PO'> Porção </option>
							<option value='PE'> Peso </option>
							<option value='PE'> Buffet (Preço Único) </option>
						</select>
					</div>
					
					<div class='col-xs-12'>
						<label>Selecione o Produto</label>
						<select name='produto'>
							<option value=''> Selecione </option>
							<option value='Canoa'> Canoa </option>
							<option value='Smiles'> Smiles </option>
							<option value='Corte Fino'> Corte Fino </option>
							<option value='Anel de Cebola'> Anel de Cebola </option>
							<option value='Palito de Mussarela'> Palito de Mussarela </option>
							<option value='Rondelles'> Rondelles </option>
							<option value='Rústica'> Rústica </option>
							<option value='Noisettes'> Noisettes </option>
							<option value='Caseira'> Caseira </option>
							<option value='Tradicional'> Tradicional </option>
						</select>
					</div>
					
					
					<div class='col-sm-4 col-xs-12'>
						<label>Preço McCain (R$/KG)</label>
						<input type='text' name='preco_kg' placeholder='9,99'>
					</div>
					
					<div class='col-sm-4 col-xs-12'>
						<label>Preço Venda (R$/KG)</label>
						<input type='text' name='preco_venda' placeholder='9,99'>
					</div>
					
					<div class='col-sm-4 col-xs-12 showOnlyPO'>
						<label class='showOtherProd'>Peso da Porção (g)</label>
						<label class='showOnly2Prod'>Quantidade média de unidades (kg)</label>

						<input type='text' name='peso' placeholder='584'>
						<br>
						<em class='showOtherProd'><small>Antes de Fritar</small></em>
					</div>
					
					<div class='col-sm-4 col-xs-12 showOnlyPO'>
						<label>Unidades por Porção</label>
						<input type='text' name='unidade' placeholder='15'>
					</div>

					<br clear='all'>
					<hr>
					
					<div class='col-md-12 col-xs-12' id='porPorcao'>
						<h4>Rendimento e Custos</h4>
						<div class='col-md-8'>
							<span class='titCalcResult'>Rendimento por Kg:</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult' id='rendimentoKG'></span>
						</div>
						
						<div class='col-md-8'>
							<span class='titCalcResult'>Custo da Porção (R$):</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult' id='custoPorcao'></span>
						</div>
						
					
						
						<div class='col-md-8'>
							<span class='titCalcResult'>Outros Custos (R$):</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult'>0,54</span>
						</div>
					</div>
					
					
					<div class='col-md-12 col-xs-12' id='porPeso'>
						<h4>Rendimento e Custos</h4>
						<div class='col-md-8'>
							<span class='titCalcResult'>Perda na Fritura (<span id='percPerdaFritura'></span>%):</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult' id='perdaFritura'></span>
						</div>
						
						<div class='col-md-8'>
							<span class='titCalcResult'>Custo final na cuba (R$/kg):</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult' id='custoCuba'></span>
						</div>
						
						
						
						<div class='col-md-8'>
							<span class='titCalcResult'>Outros Custos (R$):</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult'>0,54</span>
						</div>
					</div>
					
					
					<div class='col-md-12 col-xs-12'>
						<h4>Lucro</h4>
						
						<div class='col-md-8'>
							<span class='titCalcResult'>Lucro (R$):</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult' id='lucroVal'></span>
						</div>
						
						<div class='col-md-8'>
							<span class='titCalcResult'>Lucro (%):</span>
						</div>
						<div class='col-md-4'>
							<span class='valCalcResult' id='lucroPerc'></span>
						</div>
					</div>
					
					
					
				</div>
				
				
			</div>
			
			
		
			
			
			<br clear='all'>
		</div>
		
		<div class='footer'>
		
			
			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Política de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script>
		var perdaFritura = 20;
		
		function _RecalcAll()
		{
			if($("select[name=como_serve]").val() == '' ) return false;
			if($("select[name=produto]").val() == '' ) return false;
			
			if($("select[name=como_serve]").val() == 'PO')
				{
					if($("select[name=produto]").val() == 'Smiles' || $("select[name=produto]").val() == 'Noisettes')
					{
						var rendimentoKG = (parseFloat($("input[name=peso]").val()) / parseFloat($("input[name=unidade]").val())).toFixed(2);
						
					}
					else
					{
						var rendimentoKG = (1000/parseFloat($("input[name=peso]").val())).toFixed(2);
					}
					
					if(isNaN(rendimentoKG) || !isFinite(rendimentoKG)) rendimentoKG = 0;
					
					$("#rendimentoKG").html(rendimentoKG);
					
					var custoPorcao = ((parseFloat($("input[name=preco_kg]").val()) + 0.54) / rendimentoKG).toFixed(2);
					if(isNaN(custoPorcao) || !isFinite(custoPorcao)) custoPorcao = 0;
					
					$("#custoPorcao").html(custoPorcao);
					
					var lucroVal = parseFloat($("input[name=preco_venda]").val()) - custoPorcao;
					if(isNaN(lucroVal) || !isFinite(lucroVal)) lucroVal = 0;
					
					$("#lucroVal").html(lucroVal);
					
					var lucroPerc = ((lucroVal / custoPorcao)*100).toFixed(2);
					if(isNaN(lucroPerc) || !isFinite(lucroPerc)) lucroPerc = 0;
					
					$("#lucroPerc").html(lucroPerc);
				}
				else
				{
					
					var perdaFritura = parseFloat($("input[name=preco_kg]").val()) + (parseFloat($("input[name=preco_kg]").val())*0.05);
					if(isNaN(perdaFritura)) perdaFritura = 0;
					
					$("#perdaFritura").html(perdaFritura);
					
					var custoCuba = (0.54 + perdaFritura).toFixed(2);
					if(isNaN(custoCuba) || !isFinite(custoCuba)) custoCuba = 0;
					
					$("#custoCuba").html(custoCuba);
					
					var lucroVal = parseFloat($("input[name=preco_venda]").val()) - custoCuba;
					if(isNaN(lucroVal) || !isFinite(lucroVal)) lucroVal = 0;
					
					
					$("#lucroVal").html(lucroVal);
					
					var lucroPerc = ((lucroVal / custoCuba)*100).toFixed(2);
					if(isNaN(lucroPerc) || !isFinite(lucroPerc)) lucroPerc = 0;
					$("#lucroPerc").html(lucroPerc);
				}
		}
		
		
		$(document).ready(function(){
			$("#porPeso,#porPorcao,.showOnlyPO,.showOtherProd,.showOnly2Prod").hide();
			
			$("select[name=como_serve]").change(function(){
			
				if($(this).val() == 'PO')
				{
					$("#porPeso").hide();
					$("#porPorcao, .showOnlyPO,.showOtherProd").show();
				}
				else
				{
					$("#porPorcao, .showOnlyPO").hide();
					$("#porPeso").show();
				}
				
				_RecalcAll();
				
			});
			
			$("select[name=produto]").change(function(){
			
				if($(this).val() == 'Smiles' || $(this).val() == 'Noisettes')
				{
					perdaFritura = 5;
					$(".showOtherProd").hide();
					$(".showOnly2Prod").show();
					
				}
				else
				{
					perdaFritura = 20;
					$(".showOnly2Prod").hide();
					$(".showOtherProd").show();
					
				}
				
				$("#percPerdaFritura").html(perdaFritura);
				
				_RecalcAll();
			
			});
			
			$(".calculadora").find('input').keyup(function(){
				$(this).val($(this).val().toString().replace(/\,/g, '.'));
				
				_RecalcAll();
				
				
			});
			
			
			
			
		});
	</script>
	
		<script>
		$(document).ready(function(){
			$(".btnBusca").click(function(){
				
				if($.trim($("#busca").val()) != "")
					location.href="resultado-busca.php?q="+$.trim($("#busca").val());
			})
			
			$('#busca').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBusca').trigger('click');
				return false;
			  }
			});
		})
	</script>
  </body>
</html>
