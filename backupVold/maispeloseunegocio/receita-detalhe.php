<?php

		$dadosFull 		= json_decode(file_get_contents("json/receitas.json"),true);
		$produtosFull 	= json_decode(file_get_contents("json/produtos.json"),true);
		
		
		
		$dados = $dadosFull[$_GET[receita]];
		
		unset($dadosFull[$_GET[receita]]);
		$arrayKeys = array_rand($dadosFull,5);
		
		
		foreach($arrayKeys as $key)
		{
			$newArray[$key] = $dadosFull[$key];
		}
		
		
		$prod = array_search("1650",$produtosFull);
		foreach($produtosFull as $key => $row)
		{
			if($row[ID] == $dados[post_meta][produto])
			{
				$produto = $produtosFull[$key];
				break;
			}
		}
		
		
		

		function _StringSize($str)
	{
		
		if(strlen($str) > 50)
			return substr($str,0,55)." ...";
		else
			return $str;
		
	}

		$media = json_decode(file_get_contents("http://mccainfoodservice.com.br/wp-json/media/".($dados[post_meta][imagem])),true);
		
		

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>McCain Receitas - <?php echo ($dados[title]);?></title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<style>
@media print {
    .lateral{display:none}
    .sidebarRight, .header, .footer, .btns{display:none}
	.imgForPrint{display:block!important;margin:0 0 25px 0}
}
</style>

  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<div class='col-xs-4 col-sm-3 text-left'>
				<a href='http://mccainfoodservice.com.br'><img class='logo' src='img/logo.png'></a>
				<div class='hidden-xs selo'>
					<a href='./'><img src='img/selo_topo.png'></a>
				</div>
			</div>
			<div class='col-xs-8 col-sm-6 text-left mnDown'>
				<a href='./'>
					<div class='backHome'>
						<img src='img/menu_name.png'>
					</div>
				</a>
				<a href='receitas.php'>
					<div class='bread'>
						<img src='img/menu_rec.png' >
					</div>
				</a>
			</div>
			<div class='col-md-1 hidden-xs hidden-sm'></div>
			<div class='hidden-xs col-sm-3 col-md-2 text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div>	
		</div>
		
		<div class='content'>
			<div class='sidebar col-sm-3'>
				<div class="col-xs-6 col-sm-12 col-xs-offset-3 col-sm-offset-0 text-center">
					<img src='img/secao_rec.png' class='agenteFaz text-center'>
				</div>
				<div class="col-xs-12">
					<p>Procurando a receita de sucesso? Só aqui tem várias.</strong></p>				
				</div>
				<div class='col-xs-8 col-sm-12 col-xs-offset-2 col-sm-offset-0 text-right sidebarBusca'>
					<input type='text' id="receita" placeholder="Digite sua busca" value="<?php echo $_GET[q];?>"><button><i class='fa fa-search btnBuscaReceita'></i></button>
				</div>

			</div>
			
			<div class='col-sm-6 itemAberto' id="itemAberto">
				
				<div class='col-sm-12 item' id="itemToDownload">
					<img src="img/logo.png" class="hidden-sm hidden-xs hidden-md hidden-lg imgForPrint">
					<h2><?php echo ($dados[title]);?></h2>
					<div class='featured-img'>
						<img src='<?php echo ($media[guid]);?>' >
					</div>
					<br clear='all'>
					<div class='col-md-3 pull-right text-right btns'>
						<a href="#" id="download"><img src='img/download.png'></a> &nbsp; 
						<a href="#" id="print" onClick="window.print()"><img src='img/PRINT.png'></a>
					</div>
					
					<div class='col-md-10'>
						<h4>Ingredientes</h4>
						<p><?php echo ($dados[post_meta][ingredientes]);?></p>
						<h4>Preparo</h4>
						<p><?php echo ($dados[post_meta][preparo]);?></p>
						<h4>Rendimento</h4>
						<p><?php echo ($dados[post_meta][rendimento]);?></p>
						<h4>Indicado</h4>
						<p><?php echo ($dados[post_meta][indicado]);?></p>
					</div>
					<br clear='all'>
					
					
					
				</div>
				
				
			</div>
			
			
			<div class='col-sm-3 sidebarRight' >
				<a href="<?php echo ($produto[link]);?>"><div class='item'>
					<h2>Qual Batata combina?</h2>
					<img src='<?php echo $produto[featured_image][guid];?>' class='combineTo'>
					<h3><?php echo ($produto[title]);?>
				</div></a>
				
				<div class='item' >
					<h2>Outras Receitas</h2>
					<?php
						$cont = 0;
						foreach($newArray as $key => $row)
						{
					?>
					<div class='item-list'>
						<a href='receita-detalhe.php?receita=<?php echo $key;?>'>
						<div class='col-md-4 '><div class="thumbHeightDef"><img src='<?php echo ($row[featured_image][guid]);?>'></div></div>
						<div class='col-md-8 bd-top'><?php echo _StringSize(($row[title]));?></div>
						</a>
					</div>
					<br clear='all'>
					<?php 
					
					$cont++;
					if($cont > 5) break;
					} ?>
					
					<br clear='all'>
					
				</div>
			</div>
			
			
			
			<br clear='all'>
		</div>
		
		<div class='footer'>

			
			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Política de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/html2canvas.min.js"></script>
		<script>
		$(document).ready(function(){
			$(".btnBuscaReceita").click(function(){
				
				if($.trim($("#receita").val()) != "")
					location.href="receitas.php?q="+$.trim($("#receita").val());
			});
			
			$('#receita').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBuscaReceita').trigger('click');
				return false;
			  }
			});
		})
	</script>
	
	
	   <script src="js/bootstrap.min.js"></script>
		<script>
		$(document).ready(function(){
			$(".btnBusca").click(function(){
				
				if($.trim($("#busca").val()) != "")
					location.href="resultado-busca.php?q="+$.trim($("#busca").val());
			})
			
			$('#busca').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBusca').trigger('click');
				return false;
			  }
			});
		})
	</script>
  </body>
</html>
<script>
	$(document).ready(function(){
		$("#download").click(function(e){
				
			e.preventDefault();
		$(".imgForPrint").show();
		$("#download, #print").hide();
				html2canvas($("#itemToDownload"), {
            onrendered: function(canvas) {
				var a = document.createElement('a');
				
				a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
				a.download = '<?= (($dados[title]));?>.jpg';
				a.click();
				a.remove();
				$(".imgForPrint").hide();
		$("#download, #print").show();
                

                
                // Clean up 
                //document.body.removeChild(canvas);
            }
        });
		
		})
		
	})
</script>
