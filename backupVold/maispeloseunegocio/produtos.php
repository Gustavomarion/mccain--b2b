<?php

		$dados = json_decode(file_get_contents("json/produtos.json"),true);
		

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Fotos dos Produtos McCain</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<div class='col-xs-4 col-sm-3 text-left'>
				<a href='http://mccainfoodservice.com.br'><img class='logo' src='img/logo.png'></a>
				<div class='hidden-xs selo'>
					<a href='./'><img src='img/selo_topo.png'></a>
				</div>
			</div> 			
			<div class='col-md-4 col-sm-6 col-xs-8  text-left mnDown'>
				<a href='./'>
					<div class='backHome'>
						<img src='img/menu_name.png'>
					</div>
				</a>
			</div>			
			<div class='col-sm-4 col-md-3 hidden-xs hidden-sm'></div>
			<div class='hidden-xs col-sm-3 col-md-2 text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div> 
		</div>
		
		<div class='content'>
			<div class='sidebar col-sm-3'>
				<div class="col-xs-6 col-sm-12 col-xs-offset-3 col-sm-offset-0 text-center">
				<img src='img/secao_fotos.png' class='agenteFaz text-center'>
				</div>
				<div class="col-xs-12">
					<p>Fotos de produtos prontas para você imprimir e usar em seu estabelecimento. Afinal, consumidor também come com os olhos.</p>
				</div>
			</div>
			
			<div class='col-sm-9 homeMainArea' >
				<h2 style="color:#eee;margin-left:15px">Produtos</h2>
				<?php
					foreach($dados as $key => $row)
					{
						$media = file_get_contents("json/zipFile-".($row[post_meta][prodImgDwn]).".txt",true);
				?>
				<a href='<?php echo ($media);?>' target='_blank'><div class='col-sm-4 col-xs-12 menuItem'>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
						<img src='<?php echo ($row[featured_image][guid]);?>' class="display">
						</div>
						<hr>
						
						
						<div class='col-md-10  col-xs-10 tituloReceita'>
							<?php echo ($row[title]);?>
						</div>
						<div class='col-md-2 col-xs-2 tituloReceita'>
							<img src='img/download.png' style='max-width:100%'>
						</div>
					</div>
				</div></a>
				
					<?php } ?>
					
					<br clear="all">
					<hr>
					<h2 style="color:#eee;margin-left:15px">Embalagens</h2>
				<?php
					foreach($dados as $key => $row)
					{
						if($row[post_meta][prodImgEmb] != "")
						{
						$media = file_get_contents("json/zipFileEmb-".($row[post_meta][prodImgEmb]).".txt",true);
				?>
				<a href='<?php echo ($media);?>' target='_blank'><div class='col-sm-4 col-xs-12 menuItem'>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
						<img src='<?php echo ($media);?>' class="display">
						</div>
						<hr>
						
						
						<div class='col-md-10  col-xs-10 tituloReceita'>
							<?php echo ($row[title]);?>
						</div>
						<div class='col-md-2 col-xs-2 tituloReceita'>
							<img src='img/download.png' style='max-width:100%'>
						</div>
					</div>
				</div></a>
				
					<?php } }  ?>
				
				
				
			</div>
			<br clear='all'>
		</div>
		
		<div class='footer'>

			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Política de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
		<script>
		$(document).ready(function(){
			$(".btnBusca").click(function(){
				
				if($.trim($("#busca").val()) != "")
					location.href="resultado-busca.php?q="+$.trim($("#busca").val());
			})
			
			$('#busca').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBusca').trigger('click');
				return false;
			  }
			});
		})
	</script>
  </body>
</html>
