<?php

	$opts = array('http' => array('header' => 'Accept-Charset: UTF-8, *;q=0'));
	$context = stream_context_create($opts);
	//$dados = file_put_contents("json/livros.json",file_get_contents("http://mccainfoodservice.com.br/wp-json/posts?type=livro_receita",false,$context));
	//$dados = file_put_contents("json/culinaria.json",file_get_contents("http://mccainfoodservice.com.br/wp-json/posts?type=videos",false,$context));
	//$dados = file_put_contents("json/produtos.json",file_get_contents("http://mccainfoodservice.com.br/wp-json/posts?type=produtos",false,$context));
	//$dados = file_put_contents("json/depoimentos.json",file_get_contents("http://mccainfoodservice.com.br/wp-json/posts?type=depoimentos",false,$context));
	//$dados = file_put_contents("json/dicas.json",file_get_contents("http://mccainfoodservice.com.br/wp-json/posts?type=dicas",false,$context));
	//$dados = file_put_contents("json/receitas.json",file_get_contents("http://mccainfoodservice.com.br/wp-json/posts?type=receitas",false,$context));
	
/*	
	$dados = json_decode(file_get_contents("json/produtos.json"),true);
	foreach($dados as $key => $row)
	{
		$media = json_decode(file_get_contents("http://mccainfoodservice.com.br/wp-json/media/".($row[post_meta][prodImgDwn])),true);
		
		$dados = file_put_contents("json/zipFile-".($row[post_meta][prodImgDwn]).".txt",($media[guid]));
		
		if($row[post_meta][prodImgEmb] != "")
		{
			$media = json_decode(file_get_contents("http://mccainfoodservice.com.br/wp-json/media/".($row[post_meta][prodImgEmb])),true);
			$dados = file_put_contents("json/zipFileEmb-".($row[post_meta][prodImgEmb]).".txt",($media[guid]));
		}
	}
	

	
*/	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>McCain - A gente faz mais pelo seu negócio</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	

  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<a href='http://mccainfoodservice.com.br'><div class='col-xs-5 col-sm-2 text-left'><img class='logo' src='img/logo.png'></div></a>
			 
			<div class='col-sm-7 col-md-8 hidden-xs'></div>
			<div class='col-xs-7 col-sm-3 col-md-2 text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div> 
		</div>
		
		<div class='content'>
			<div class='sidebar col-sm-3'>
                    <div class="row">
                        <div class="col-xs-6 col-sm-12 col-xs-offset-3 col-sm-offset-0 text-center">
                            <img src='img/agentefaz.png' class='agenteFaz'> 
                        </div>
                        <div class="col-xs-12">
				<p>Esta é uma plataforma com ferramentas muito úteis para o dia-a-dia do seu negócio, que vai ajudar o seu estabelecimento a crescer ainda mais. <strong>Acompanhe cada uma das sessões e veja as novidades</strong></p>
						</div>
                    </div>
                </div>
			
			<div class='col-sm-9 homeMainArea' >
				
				<a href='receitas.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/receitas.png' class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/rec_icon.png'  class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Receitas
						</div>
					</div>
				</div></a>
				
				<a href='calculadora.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/calculadora.png'  class="display">	
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/calculadora_icon.png'  class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Calculadora de Lucros
						</div>
					</div>
				</div></a>
				
				<a href='culinaria.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/cozinha.png'  class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/play_icon.png'  class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Cozinha McCain
						</div>
					</div>
				</div></a>
				
				
				<a href='depoimentos.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/parceria.png'  class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/parceria_icon.png'  class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Parcerias de sucesso
						</div>
					</div>
				</div></a>
				
				<a href='produtos.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/down.png'  class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/fotos_icon.png'  class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Baixar fotos
						</div>
					</div>
				</div></a>
				
				<a href='dicas.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/dicas.png'  class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/dicas_icon.png'  class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Dicas para o seu negócio
						</div>
					</div>
				</div></a>
				
				<a href='contato.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/alo.png'  class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/alo_icon.png' class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Alô, McCain
						</div>
					</div>
				</div></a>


				<a href='livros.php'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/livros-capa.jpg'  class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/livro-icon.png' class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Livros de Receitas
						</div>
					</div>
				</div></a>
				

				<a href='http://mccainfoodservice.com.br/consultor-mccain/'><div class='col-sm-4 col-xs-12 menuItem '>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
							<img src='img/consultor.png'  class="display">
						</div>
						<hr>
						<div class='col-xs-3 col-sm-4 icone'>
							<img src='img/icone.png' class='ico'>
						</div>
						
						<div class='col-xs-9 col-sm-8 titulo'>
							Consultor McCain
						</div>
					</div>
				</div></a>
				
			</div>
			<br clear='all'>
		</div>
		
		<div class='footer'>

			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Política de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
	<script>
		$(document).ready(function(){
			$(".btnBusca").click(function(){
				
				if($.trim($("#busca").val()) != "")
					location.href="resultado-busca.php?q="+$.trim($("#busca").val());
			})
			
			$('#busca').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBusca').trigger('click');
				return false;
			  }
			});
		})
	</script>
  </body>
</html>
