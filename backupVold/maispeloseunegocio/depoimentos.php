<?php

		$dados = json_decode(file_get_contents("json/depoimentos.json"),true);
		//echo "<pre>";
		//print_r($dados);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Parcerias de Sucesso McCain - A gente faz mais pelo seu negócio</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<div class='col-xs-4 col-sm-3 text-left'>
				<a href='http://mccainfoodservice.com.br'><img class='logo' src='img/logo.png'></a>
				<div class='hidden-xs selo'>
					<a href='./'><img src='img/selo_topo.png'></a>
				</div>
			</div> 			
			<div class='col-md-4 col-sm-6 col-xs-8  text-left mnDown'>
				<a href='./'>
					<div class='backHome'>
						<img src='img/menu_name.png'>
					</div>
				</a>
			</div>			
			<div class='col-sm-4 col-md-3 hidden-xs hidden-sm'></div>
			<div class='hidden-xs col-sm-3 col-md-2 text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div> 
		</div>
		
		<div class='content'>
			<div class='sidebar col-sm-3'>
				<div class="col-xs-6 col-sm-12 col-xs-offset-3 col-sm-offset-0 text-center">
					<img src='img/secao_parceria.png' class='agenteFaz text-center'>
				</div>
				<div class="col-xs-12">
					<p>Aqui você conhece um pouco mais o que a McCain pode lhe oferecer através da experiência de quem trabalha com os nossos produtos no próprio estabelecimento.<br/>Sinta um pouco o dia-a-dia dos parceiros que escolheram McCain.</p>
				</div>
			</div>
			
			<div class='col-sm-9 homeMainArea' >
				<?php
					foreach($dados as $key => $row)
					{
				?>
				<a href='depoimento-detalhe.php?depoimento=<?php echo $key;?>'><div class='col-sm-4 col-xs-12 menuItem'>
					<div class='boxMenuItem home col-md-12'>
						<div class="imgHeightDef">
						<img src='<?php echo ($row[featured_image][guid]);?>' class="display">
						</div>
						<hr>
						
						
						<div class='col-md-12 tituloReceita'>
							<?php echo ($row[title]);?>
						</div>
					</div>
				</div></a>
				
					<?php } ?>
				
				
				
			</div>
			<br clear='all'>
		</div>
		
		<div class='footer'>

			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Política de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
	<script>
		$(document).ready(function(){
			$(".btnBusca").click(function(){
				
				if($.trim($("#busca").val()) != "")
					location.href="resultado-busca.php?q="+$.trim($("#busca").val());
			})
			
			$('#busca').keypress(function (e) {
			  if (e.which == 13) {
				$('.btnBusca').trigger('click');
				return false;
			  }
			});
		})
	</script>
  </body>
</html>
