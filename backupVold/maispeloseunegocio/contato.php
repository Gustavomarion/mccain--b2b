<?php

	if($_GET[sendEmail] == "true")
	{
		
			
			$deQuem=$_GET[email];
			$headers = "Return-Path: ".$deQuem."\n";
			$headers .= "X-Sender: ".$deQuem."\n";
			$headers .= "From: ".$deQuem."\n";
					
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

			$mensagem = "
			
				Nome: ".$_GET[nome]." ".$_GET[sobrenome]."<Br>
				E-mail: ".$_GET[email]."<Br>
				
				Assunto: ".$_GET[assunto]."<Br>
				Mensagem: ".$_GET[msg]."<Br>
			";
			mail("alomccain@mccain.com.br","Novo contato",($mensagem),$headers);
			
			die();
	}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Contato McCain - A gente faz mais pelo seu negócio</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	

  </head>
  <body>

	<div class='container-liquid'>
		<div class='header'>
			<div class='col-xs-4 col-sm-3 text-left'>
				<a href='http://mccainfoodservice.com.br'><img class='logo' src='img/logo.png'></a>
				<div class='hidden-xs selo'>
					<a href='./'><img src='img/selo_topo.png'></a>
				</div>
			</div>
			<div class='col-md-4 col-sm-6 col-xs-8  text-left mnDown'>
				<a href='./'>
					<div class='backHome'>
						<img src='img/menu_name.png'>
					</div>
				</a>
			</div>			
			<div class='col-sm-4 col-md-3 hidden-xs hidden-sm'></div>
			<div class='hidden-xs col-sm-3 col-md-2 text-right busca'><input type='text' id="busca" placeholder="Digite sua busca"><button class="btnBusca"><i class='fa fa-search'></i></button></div> 
		</div>
		
		<div class='content'>
			<div class='sidebar col-sm-3'>
				<div class="col-xs-6 col-sm-12 col-xs-offset-3 col-sm-offset-0 text-center">
					<img src='img/contato.png' class='agenteFaz text-center'>
				</div>
				<div class="col-xs-12">
					<p>Tem alguma dúvida, sugestão ou crítica?<br>Ligue gratuitamente para a gente. Ficaremos felizes em ajudar o seu estabelecimento a crescer ainda mais.</p>
					<h3>Alô McCain! 0800-200 8080</h3>
					<p>Horário de atendimento de segunda à sexta feira das 9h às 18h</p>
					<p>Se preferir, envie um e-mail para <a href='mailto:alomccain@mccain.com.br' class='linkContato'>alomccain@mccain.com.br<a></p>
				</div>
			</div>
			
			<div class='col-sm-9 col-md-6 itemAberto' >
				<form name="formContato" id="formContato" class="formContato">
				<div class='col-md-12 item contato'>
					
					<div class='col-sm-6'>
						<label>Nome</label>
						<input type='text' name='nome' placeholder='Nome' class="required">
					</div>
					
					<div class='col-sm-6'>
						<label>Sobrenome</label>
						<input type='text' name='sobrenome' placeholder='Sobrenome' class="required">
					</div>
					
					<br clear='all'>
					
					<div class='col-md-12'>
						<label>E-mail</label>
						<input type='email' name='email' placeholder='Digite seu e-mail' class="required">
					</div>
					
					<br clear='all'>
					
					<div class='col-md-12'>
						<label>Assunto</label>
						<input type='text' name='assunto' placeholder='Digite o assunto' class="required">
					</div>
					
					<br clear='all'>
					
					<div class='col-md-12'>
						<label>Mensagem</label>
						<textarea name='msg' placeholder='Digite sua mensagem' class="required"></textarea>
					</div>
					
					<div class='col-md-2'>
						<button id="btSend" type="button">Enviar</button>
					</div>
					
				</div>
				</form>
				
				
			</div>
			
			<!--
			<div class='col-md-3 sidebarRight' >
				<div class='item'>
					
					
					<br><br><br><br><br><br>
					<br><br><br><br><br><br>
				</div>
				
				
			</div>
			-->
			
			
			<br clear='all'>
		</div>
		
		<div class='footer'>
		
			
			
			<div class='text-center copyright'>
				Copyright &copy; 2016 McCain do Brasil. Todos os direitos reservados. Política de Privacidade  <img class='logoBt' src='img/logo.png'>
			</div>
			
		</div>
	</div>
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
		<script>
		$(document).ready(function(){
			
			$("#btSend").click(function(){
				
				var ok = true;
				
				$(".required").each(function(){
					
					if($.trim($(this).val()) == "")
						ok = false;
				});
				
				if(!ok)
				{
					alert("Por favor, preencha todos os campos.");
					return false;
				}
				
				
				$.post("contato.php?sendEmail=true&"+$(".formContato").serialize(),function(){
				
					$(".required").val('');
					
					alert("Mensagem enviada com sucesso.");
					
				})
				
			});
			
			
			});
	</script>
  </body>
</html>
