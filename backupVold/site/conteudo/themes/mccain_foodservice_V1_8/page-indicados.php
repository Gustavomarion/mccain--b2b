<?php
/*
Template Name: Produtos indicados
*/
?>
<?php get_header(); ?>
<?php get_sidebar('top'); ?>
			<?php

			if (have_posts()) {
				/* Start the Loop */
				while (have_posts()) {
					the_post();
					get_template_part('content', 'page');
				}
			} else {
				theme_404_content();
			}
			?>

<br>
<br>
<?php
  
  $categories = get_categories('taxonomy=estabelecimentos');
  
  $select = "<select name='cat' id='cat' class='postform'>n";
  $select.= "<option value='-1'>Escolha o tipo de estabelecimento</option>n";
  
  foreach($categories as $category){
    if($category->count > 0){
        $select.= "<option value='".$category->slug."'>".$category->name."</option>";
    }
  }
  
  $select.= "</select>";
  
  echo $select;
?>

<script type="text/javascript">
    var dropdown = document.getElementById("cat");
    function onCatChange() {
        if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
            
$('#blmain').load ("<?php echo home_url();?>/estabelecimentos/"+dropdown.options[dropdown.selectedIndex].value+"/");       

}
    }
    dropdown.onchange = onCatChange;
</script>

<div id="blmain" class="bl_cont"></div>

			<?php get_sidebar('bottom'); ?>
             
<?php get_footer(); ?>