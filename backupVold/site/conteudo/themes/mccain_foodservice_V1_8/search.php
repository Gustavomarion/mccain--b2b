<?php

/**
 *
 * search.php
 *
 * The search results template. Used when a search is performed.
 *
 */
get_header();
?>
<div class="bt_not">
			<?php get_sidebar('top'); ?>
			<?php
			if (have_posts()) {
				theme_post_wrapper(
						array('content' => '<h4 class="box-title">' . sprintf(__('Resultados da busca por: %s', THEME_NS), '<span class="search-query-string">' . get_search_query() . '</span>') . '</h4>'
						)
				);
				/* Display navigation to next/previous pages when applicable */
				if (theme_get_option('theme_top_posts_navigation')) {
					theme_page_navigation();
				}
				/* Start the Loop */
				while (have_posts()) {
					the_post();
					get_template_part('content', 'search');

				}
				/* Display navigation to next/previous pages when applicable */
				if (theme_get_option('theme_bottom_posts_navigation')) {
					theme_page_navigation();
				}
			} else {
?></div><div class="bl_home"><?php				
theme_404_content(
						array(
							'error_title' => __('Não encontrado', THEME_NS),
							'error_message' => __('Desculpe, não encontramos nada. Por favor, tente com outras palavras.', THEME_NS)
						)
				);
			}
			?></div>
			<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>