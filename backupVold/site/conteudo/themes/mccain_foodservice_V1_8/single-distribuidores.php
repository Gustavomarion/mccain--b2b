<?php 

/**
 *
 * single-distribuidores.php
 *
 * The single post template. Used when a single post is queried.
 * 
 */	


get_header(); ?>

<div class="bl_home">

<div class="bl_tit"> 
	<h1>Distribuidores</h1>
<a style="float: right;" class="mc-button" href="javascript:history.go(-1);">&lt; VOLTAR</a>	
</div>

<div class="bl_main"> 
	<div class="dist_tit"><?php the_title(); ?></div>


<p>		
<?php if(get_custom_field('distarea')){?>
<strong>Região de atuação:</strong> <?php print_custom_field('distarea:formatted_list');?><br />
<?php
}
?>

<?php if(get_custom_field('distrnome')){?>
<strong>Telefone: </strong> <?php print_custom_field('distrnome:formatted_list');?><br />
<?php
}
?>
</p>

<div class="dist_mail">
<?php if(get_custom_field('distemail')){?>
<strong>E-mail: </strong> <a href="mailto:<?php print_custom_field('distemail:formatted_list');?>"><?php print_custom_field('distemail:formatted_list');?></a><br />
<?php
}
?>
</div>

</div>
</div>
<?php get_footer(); ?>

		