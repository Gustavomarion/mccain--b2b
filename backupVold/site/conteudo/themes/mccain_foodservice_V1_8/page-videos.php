<?php
/*
Template Name: Videos
*/
?>
<?php get_header(); ?>
<?php get_sidebar('top'); ?>
	<?php

		if (have_posts()) {
			/* Start the Loop */
			while (have_posts()) {
				the_post();
				get_template_part('content', 'page');
				}
			} else {
				theme_404_content();
			}
	?>
	
	<?php
 if( is_user_logged_in() ) { ?>			
	
				<div id="slider3" class="flexslider"> 
			
				<ul class="slides">
					<?php query_posts(array('post_type' => 'videos','order' => 'ASC', 'orderby' => 'menu_order' ));
						 
						if(have_posts()) : while(have_posts()) : the_post();
						
						?>
						
							<li class="slide play3">
								
								<div class="sli_vid">	
								<?php print_custom_field('vidcode:do_shortcode');   ?>
								</div>
								<div class="sli_info">
								<h1><?php the_title(); ?></h1>
									<?php the_content(); ?>	
									
								</div>
							</li>
					<?php endwhile; endif; wp_reset_query(); ?>
        		</ul>
    		
 		</div>


<div class="vidcarousel">
	<h2>Assista outros vídeos:</h2>
</div>

<div id="carousel" class="flexslider">
	
	
	<ul class="slides">
									<?php query_posts(array('post_type' => 'videos','order' => 'ASC', 'orderby' => 'menu_order' ));
						 
						if(have_posts()) : while(have_posts()) : the_post();
						
						?>
						
							<li>
							<div class="vidbox">
							<div class="setaplay"></div>
							<div class="nomeplay"><?php the_title(); ?></div>
							</div>
							<?php echo the_post_thumbnail('thumbnail');?>
							
							<div class="nomeplay"><?php the_title(); ?></div>
							</li>
									
									
									<?php //echo do_shortcode ('[summarize_posts post_type="videos"]<li><img src=[+thumbnail_src:thumbnail+]></li>[/summarize_posts]') ?>
								<?php endwhile; endif; wp_reset_query(); ?>
							
					        		</ul>
</div>

<?php } ?>
			
	<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>