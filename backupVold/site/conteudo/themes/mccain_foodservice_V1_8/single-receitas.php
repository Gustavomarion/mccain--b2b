<?php 

/**
 *
 * single-receitas.php
 *
 * The single post template. Used when a single post is queried.
 * 
 */	

get_header(); ?>



<div class="bl_cont">

<div class="bl_main">

<div class="bl_tit">
	<h1>Receitas</h1>
<a style="float: right;" class="mc-button" href="javascript:history.go(-1);">&lt; VOLTAR</a>
	<h2><?php the_title(); ?></h2>
 </div>
	<div class="imgrec"><img src="<?php print_custom_field('recimg:to_image_src'); ?>"></div>
	<div class="listrec1"><h3>Ingredientes</h3><br/><?php print_custom_field('recing:do_shortcode'); ?></div>
	<div class="listrec2"><h3>Preparo</h3><br/><?php print_custom_field('recprep'); ?></div>
	

<?php
    $rendprep = get_custom_field('recrend');

    if ( ! empty ( $rendprep ) )
    {?>
<div class="listrec3">
<h3>Rendimento</h3><br/><?php print_custom_field('recrend'); }?>

</div>
 
</div>


<div class="bl_side">


<?php
    $batcomb = get_custom_field('recprod');

    if ( ! empty ( $batcomb ) )
    {?>

	<div class="imgbatcomb">

<div class="callimg"></div>

<?php
$my_post = get_custom_field('recprod:get_post');
$image_id = $my_post['prodimgprt'];


?>
<a href="<?php print $my_post['guid']; ?>">
<img src="<?php print CCTM::filter($image_id, 'to_image_src'); ?>" /></a>
<h3> <?php print $my_post['post_title']; ?> </h3>

</div>
<?php } ?>

	<div class="tabindpara">
<div class="taband"><p>INDICADO PARA</p></div>

<?php print_custom_field('induse:do_shortcode'); ?>

<div class="bt_indic">
<p>Quer saber quais batatas McCain são<br>indicadas para o seu tipo de negócio?</p>

<a href="<?php echo home_url(); ?>/produtos-indicados"><button class="mc-button">
<strong>Clique aqui</strong></button>
</a>
</div>
</div>
</div>
	
</div>

	


<?php get_footer(); ?>