<?php
/**
 *
 * taxonomy-estabelecimento.php
 *
 * The archive template. Used when a category, author, or date is queried.
 * Note that this template will be overridden by category.php, author.php, and date.php for their respective query types. 
 *
 * More detailed information about template’s hierarchy: http://codex.wordpress.org/Template_Hierarchy
 *
 */
?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" media="screen" />

<div class="gridthumbs">
<ul>
<?php if ( have_posts() ){

query_posts($query_string . "&orderby=title&order=ASC");

while ( have_posts() ) : the_post(); ?>

<li>

<div class="gridthubox"><a href="<?php echo get_permalink(); ?>"><img src=<?php print_custom_field('prodimgprt:to_image_src:thumbnail') ?>></a></div>
<p class="prod"><?php the_title(); ?></p>

</li>

<?php endwhile; } ?>
</ul>
</div>
