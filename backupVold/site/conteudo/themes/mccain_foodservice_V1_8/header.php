<?php global $wp_locale;
if (isset($wp_locale)) {
	$wp_locale->text_direction = 'ltr';
} ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset') ?>" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<!-- Created by Artisteer v4.2.0.60623 -->
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width" />
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/flexslider.css" type="text/css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/jquery.flexslider-min.js"></script>
    <script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
    
<script type="text/javascript">(function($) {
    $(window).load(function() {
        $('#slider .flexslider').flexslider({
               animation: 'slide',
                slideshowSpeed: 9000,
                animationSpeed: 900,
                pauseOnAction: true,
                pauseOnHover: true,
                controlNav: false,
                directionNav: true, 
                controlsContainer: ".flexslider",
        });
    });
    
    
 

$(window).ready(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 210,
    itemMargin: 5,
    asNavFor: '#slider3'
  });
 
  $('#slider3').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});
  
   })(jQuery)
</script>
<!--
<script type="text/javascript">(function($) {
// Youtube and Vimeo Flexslider Control
// Partly from Duppi on GitHub, partly from digging into the YouTube API
// Need to set this up to use classes instead of IDs

var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

$(window).load(function() {
    $('#slider3 .flexslider3').flexslider({
        useCSS: false,
        animation: "slide",
        animationLoop: true,
        slideshow: false,
        pauseOnAction: true,
        pauseOnHover: true,
        controlNav: "thumbnails",
        video: true,
        directionNav: false,
        controlsContainer: ".control_thu"
        
    });

var vimeoPlayers = $('#slider3 .flexslider3').find('iframe'), player;

for (var i = 0, length = vimeoPlayers.length; i < length; i++) {
        player = vimeoPlayers[i];
        $f(player).addEvent('ready', ready);
}

function addEvent(element, eventName, callback) {
    if (element.addEventListener) {
        element.addEventListener(eventName, callback, false)
    } else {
        element.attachEvent(eventName, callback, false);
    }
}

function ready(player_id) {
    var froogaloop = $f(player_id);
    froogaloop.addEvent('play', function(data) {
     $('#slider3 .flexslider3').flexslider("pause");
    });

    froogaloop.addEvent('pause', function(data) {
        $('#slider3 .flexslider3').flexslider("play");
    });
}

$("#slider3 .flexslider3")

.flexslider({
    before: function(slider){
        if (slider.slides.eq(slider.currentSlide).find('iframe').length !== 0)
           $f( slider.slides.eq(slider.currentSlide).find('iframe').attr('id') ).api('pause');
           /* ------------------  YOUTUBE FOR AUTOSLIDER ------------------ */
           playVideoAndPauseOthers($('.play3 iframe')[0]);
    }


});

function playVideoAndPauseOthers(frame) {
$('iframe').each(function(i) {
var func = this === frame ? 'playVideo' : 'stopVideo';
this.contentWindow.postMessage('{"event":"command","func":"' + func + '","args":""}', '*');
});
}

/* ------------------ PREV & NEXT BUTTON FOR FLEXSLIDER (YOUTUBE) ------------------ */
/*
$('.flex-next, .flex-prev').click(function() {
playVideoAndPauseOthers($('.play3 iframe')[0]);
});

*/

//YOUTUBE STUFF

function controlSlider(event) {
    console.log(event);
    var playerstate=event.data;
    console.log(playerstate);
    if(playerstate==1 || playerstate==3){
        $('#slider3 .flexslider3').flexslider("pause");
    };
    if(playerstate==0 || playerstate==2){
        $('#slider3 .flexslider3').flexslider("play");
    };
};

var player = new YT.Player('youtubevideo', {
    events: {
      'onReady': function(event) { console.log(event); },
      'onStateChange': function(event) { controlSlider(event); }
    }
  }); 
});
 })(jQuery)
</script>
-->
<?php
remove_action('wp_head', 'wp_generator');
if (is_singular() && get_option('thread_comments')) {
	wp_enqueue_script('comment-reply');
}
wp_head();
?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72691338-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body <?php body_class(); ?>>


<div id="mc-main">

<?php if(theme_has_layout_part("header")) : ?>

<header class="mc-header">

<?php get_sidebar('header'); ?>


</header>
<?php endif; ?>
 
<?php if (theme_get_option('theme_use_deafult_menu')) { wp_nav_menu( array('theme_location' => 'primary-menu') );} else { ?><nav class="mc-nav">



   <div class="mc-nav-inner">
<div class="bg_amarelo"></div>
    <?php
	echo theme_get_menu(array(
			'source' => theme_get_option('theme_menu_source'),
			'depth' => theme_get_option('theme_menu_depth'),
			'menu' => 'primary-menu',
			'class' => 'mc-hmenu'
		)
	);
	
?> 
        </div>
    </nav><?php } ?>
<?php
if ( is_front_page() ) {?>
<section id="slider"> 
      <div class="flexslider">
        <ul class="slides">
        <?php query_posts(array('post_type' => 'home_slide','order' => 'ASC', 'orderby' => 'menu_order' )); if(have_posts()) : while(have_posts()) : the_post();?>
            <li class="slide">
            <?php if (get_custom_field ('blnkopt')==1){ ?>
            <a href="<?php print_custom_field('url_banner')?>" target="_blank"> 
            <?php } else {  ?>
             <a href="<?php print_custom_field('url_banner')?>"> 
            <?php
            
            };
            ?>
            <img src="<?php print_custom_field('img_banner:to_image_src');?>"/>
            </a>
          </li>
        <?php endwhile; endif; wp_reset_query(); ?>
        </ul>
    </div>
 </section> 

<?php

/*if ( is_front_page() ) {
    echo do_shortcode( "[slider id='401' name='Home']" );
*/
?>
<div class="banie"><a href="<?php echo home_url(); ?>/empresa"><img src="<?php echo home_url(); ?>/site/conteudo/themes/mccain_foodservice_V1_8/images/banie.jpg"/></a></div>
<?php } ?>


<div class="mc-sheet clearfix">
            <div class="mc-layout-wrapper">
                <div class="mc-content-layout">
                    <div class="mc-content-layout-row">
                        <div class="mc-layout-cell mc-content">

