<?php 

/**
 *
 * single-noticias.php
 *
 * The single post template. Used when a single post is queried.
 * 
 */	


get_header(); ?>

<div class="bl_cont">

<div class="bl_tit"> 
	<h1 >Notícias</h1>
<a style="float: right;" class="mc-button" href="javascript:history.go(-1);">&lt; VOLTAR</a>	
</div>

<div class="bl_main"> 
	<div class="titnot"><h2 style="padding-left: 20px;"><?php the_title(); ?></h2></div>
	<div class="textnot"><?php the_content(); ?></div>
	
<div class="bt_pdf" style="padding-left: 20px;"><?php print_custom_field('notpdfdown:get_post','<a href="[+guid+]" target="_blank"><button class="mc-button">
<strong>Clique aqui para visualizar / salvar PDF</strong></button>
</a>'); ?></div>

</div>
	


</div>

<?php get_footer(); ?>