<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>
<?php echo do_shortcode( "[slider id='401' name='Home']" ) ?>

<div class="mc-layout-wrapper">
    <div class="mc-content-layout">
        <div class="mc-content-layout-row">
            <div class="mc-layout-cell mc-content">
			<?php get_sidebar('top'); ?>

			

<?php 
				if(have_posts()) {
					
					/* Start the Loop */ 
					while (have_posts()) {
						the_post();
						get_template_part('content', 'page');
						/* Display comments */
						if ( theme_get_option('theme_allow_comments')) {
							comments_template();
						}
					}

				} else {
				
					 theme_404_content();
					 
				} 
		    ?>


			<?php get_sidebar('bottom'); ?>
              <div class="cleared"></div>
            </div>
        </div>
    </div>
</div>
<div class="cleared"></div>
<?php get_footer(); ?>