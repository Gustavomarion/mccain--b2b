<?php
/*
Template Name: Estado
*/
?>
<?php get_header(); ?>
<?php get_sidebar('top'); ?>
			<div id="lista">
<br />
<H2><?php echo category_description(); ?></H2></div>		
</br>



<?php if ( have_posts() ){

query_posts($query_string . "&orderby=title&order=ASC");

while ( have_posts() ) : the_post(); ?>
	
<div class="distlinha">

	<div class="dist_tit"><?php the_title(); ?></div>
<br/>

<p>		
<?php if(get_custom_field('distarea')){?>
<strong>Região de atuação:</strong> <?php print_custom_field('distarea:formatted_list');?><br />
<?php
}
?>

<?php if(get_custom_field('distrnome')){?>
<strong>Telefone: </strong> <?php print_custom_field('distrnome:formatted_list');?><br />
<?php
}
?>
</p>

<div class="dist_mail">
<?php if(get_custom_field('distemail')){?>
<strong>E-mail: </strong> <a href="mailto:<?php print_custom_field('distemail:formatted_list');?>"><?php print_custom_field('distemail:formatted_list');?></a><br />
<?php
}
?>
</div>

<br/>
<br/>
</div>


<?php endwhile; }// end of the loop.
else{
?>

<p>Nenhum distribuidor nesta região</p>
<?php
}

			<?php get_sidebar('bottom'); ?>
             
<?php get_footer(); ?>