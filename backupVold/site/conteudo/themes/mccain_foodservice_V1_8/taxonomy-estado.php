<?php
/**
 *
 * taxonomy-estado.php
 *
 * The archive template. Used when a category, author, or date is queried.
 * Note that this template will be overridden by category.php, author.php, and date.php for their respective query types. 
 *
 * More detailed information about template’s hierarchy: http://codex.wordpress.org/Template_Hierarchy
 *
 */
?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" media="screen" />



<div class="est_nome">



<h2><?php 

echo get_queried_object()->name;

?></h2></div>		




<?php if ( have_posts() ){

query_posts($query_string . "&orderby=title&order=ASC");

while ( have_posts() ) : the_post(); ?>
	
<div class="distlinha">

	<div class="dist_tit"><?php the_title(); ?></div>


<p>		
<?php if(get_custom_field('distarea')){?>
<strong>Região de atuação:</strong> <?php print_custom_field('distarea:formatted_list');?><br />
<?php
}
?>

<?php if(get_custom_field('distrnome')){?>
<strong>Telefone: </strong> <?php print_custom_field('distrnome:formatted_list');?><br />
<?php
}
?>
</p>

<div class="dist_mail">
<?php if(get_custom_field('distemail')){?>
<strong>E-mail: </strong> <a href="mailto:<?php print_custom_field('distemail:formatted_list');?>"><?php print_custom_field('distemail:formatted_list');?></a><br />
<?php
}
?>
</div>

<br/>
<br/>
</div>


<?php endwhile; }// end of the loop.
else{
?>

<p>Nenhum distribuidor nesta região</p>
<?php
}
?>
