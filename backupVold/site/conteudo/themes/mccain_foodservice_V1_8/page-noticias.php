<?php
/*
Template Name: Notícias
*/
?>
<?php get_header(); ?>
<?php get_sidebar('top'); ?>
			<?php

			if (have_posts()) {
				/* Start the Loop */
				while (have_posts()) {
					the_post();
					get_template_part('content', 'page');
				}
			} else {
				theme_404_content();
			}
			?>

<div class="bl_cont">


<div class="bt_not">
<?php echo do_shortcode('[summarize_posts post_type="noticias" order="DESC" orderby="post_date"]<li><a href="[+permalink+]"><h1>[+post_title+]</h1><h4>[+post_excerpt+]</h4></a></li>[/summarize_posts]') ?>
</div>



</div>

			<?php get_sidebar('bottom'); ?>
             
<?php get_footer(); ?>