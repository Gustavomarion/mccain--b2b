<?php 

/**
 *
 * single-produtos.php
 *
 * The single post template. Used when a single post is queried.
 * 
 */	


get_header(); ?>


<div class="bl_cont">
	<div class="bl_tit"> 

		<h1>Produtos</h1>
		<a style="float: right;" class="mc-button" href="javascript:history.go(-1);">&lt; VOLTAR</a>
		<h2><?php the_title(); ?></h2> 


	</div>

	<div class="bl_bar"> 
		<img src="<?php print_custom_field('prodimgprt:to_image_src'); ?>" />
	</div>

	<div class="bl_main"> 
		<div class="caract"><?php print_custom_field('prodcaract'); ?></div>

		<div class="dadtec"><h4>Dados Técnicos</h4>	<?php print_custom_field('proddadtec'); ?></div>
		<div class="imgprodemb"><img src="<?php $prod_img = get_custom_field('prodimgemb');
			if ($prod_img){print_custom_field('prodimgemb:to_image_src','thumbnail'); }?>"/></div>

			<div class="tabnut">
			<?php
			$tabnut = get_custom_field('tabnut');

			if ($tabnut){ ?>


			<?php print_custom_field('tabnut:do_shortcode');
				} ?>
			</div>

			<div class="bt_pdf"><?php print_custom_field('prodvispdf:get_post','<a href="[+guid+]" target="_blank"><button class="mc-button"><strong>Clique aqui para visualizar o PDF</strong></button></a>'); ?>	</div>
	</div>

	<div class="bl_side">
		
		<div class="bt_distr">
	
			<a href="http://mccainfoodservice.com.br/distribuidores/"> 
		
				<img src= "<?php echo get_template_directory_uri();?>/images/bt_distribuidores.jpg"/></a>
		
		</div>

		<div class="dicprep">

			<div class="callimg"></div>

				<?php print_custom_field('proddicprep:formatted_list', array('<li>[+value+]</li>','<ol>[+content+]</ol>') );?>

		</div>
		<?php

			$have_rec = get_custom_field('have_rec');
			$have_down = get_custom_field('prodimgdown');
			if($have_rec){?>
	
		<div class="linkrec"><a href="../../receitas/"><h4>Conheça nossas sugestões</h4>
			<div class="callimg"></div>
			</a>
		<!--
		<?php
		$posts_array = get_custom_field('recprod:to_array', 'get_post');
		foreach ($posts_array as $p) {
		   printf('<a href="%s">%s</a>: %s', $p['guid'], $p['post_title'], $p['my_custom_field']);
		}
		?>
		-->
		</div>

			<?php } ?>
			<?php 

				if($have_down){
					 
						print_custom_field('prodimgdown:get_post','<a href="[+guid+]" download><div class="linkdown"><div class="callimg"></div></div></a>');					
				}

				$induse = get_custom_field('induse');

					if ($induse){ ?>
						<div class="tabindpara">
							<div class="taband"><p>INDICADO PARA</p></div>
							<?php print_custom_field('induse:do_shortcode'); ?>
						</div>
							<?php } ?>
	</div>

</div>



<?php get_footer(); ?>