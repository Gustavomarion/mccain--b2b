<?php
/**
 *
 * archive.php
 *
 * The archive template. Used when a category, author, or date is queried.
 * Note that this template will be overridden by category.php, author.php, and date.php for their respective query types. 
 *
 * More detailed information about template’s hierarchy: http://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>

		<?php get_sidebar('top'); ?>




<?php


if( is_user_logged_in() ) {
?>



<div class="newsheader"><img src="<?php echo get_template_directory_uri(); ?>/images/Testeira-McCain-News.jpg"/>
	<div class="newsinfos">
		<h2><?php echo $wp_query->queried_object->name; ?></h2>
		<p> Publicação Bimestral </p>
	</div>


<?php
 
  $categories = get_categories(array('taxonomy'=>'edicoes', 'orderby'=>'name','order'=>'DESC'));
 
  $select = "<select name='cat' id='cat' class='postform'>n";
  $select.= "<option value='-1'>Selecione uma edição</option>n";
 
  foreach($categories as $category){
    if($category->count > 0){
        $select.= "<option value='".$category->slug."'>".$category->name."</option>";
    }
  }
 
  $select.= "</select>";
 
  echo $select;
?>
 
<script type="text/javascript"><!--
    var dropdown = document.getElementById("cat");
    function onCatChange() {
        if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
            location.href = "<?php echo home_url();?>/edicoes/"+dropdown.options[dropdown.selectedIndex].value+"/";
        }
    }
    dropdown.onchange = onCatChange;

var fixed = false;
$(document).scroll(function() {
    if( $(this).scrollTop() >= 228 ) {
        if( !fixed ) {
            fixed = true;
            $('.newsheader').css({position:'fixed', top:'0'});
			
        }
    } else {
        if( fixed ) {
            fixed = false;
            $('.newsheader').css({position:'absolute'});
			
			
        }
    }
})
--></script>



<?php 
echo '<ul>';
while( have_posts()){
the_post();
$product_terms = wp_get_object_terms( $post->ID,  'news' );
if ( ! empty( $product_terms ) ) {
	if ( ! is_wp_error( $product_terms ) ) {
		
			foreach( $product_terms as $term ) {

$newslug = $wp_query->queried_object->slug;
				echo '<li><a href="#' . $term->slug . '" onclick="_gaq.push([\'_trackPageview\' ,\'/edicoes/' . $newslug .'/#'. $term->slug . '/\']);">' . $term->name . '</a></li>'; 
			}
		
	}
}
}
echo '</ul>';
?>

</div>
<div class="news">

	<?php

		if (have_posts()) {
			/* Start the Loop */
			while (have_posts()) {
the_post();
$postids = wp_get_object_terms( $post->ID, 'news' );
foreach ($postids as $postid){}
 
				echo '<div id="' . $postid->slug . '">';
					echo '<div class="new">';
						get_template_part('content', 'page');
					echo '</div>';
				echo '</div>';
			}
		} else {
				theme_404_content();
		}
	?>
</div>

<?php

 
}else{
//echo do_shortcode('[wp-members page="members-area"]');
echo do_shortcode('[wp-members page="login"]');
}

get_sidebar('bottom'); ?>
<?php get_footer();
