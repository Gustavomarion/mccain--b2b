<?php
/*
Template Name: Produtos 2
*/
?>
<?php get_header(); ?>
<?php get_sidebar('top'); ?>
			<?php

			if (have_posts()) {
				/* Start the Loop */
				while (have_posts()) {
					the_post();
					get_template_part('content', 'page');
				}
			} else {
				theme_404_content();
			}
			?>
<!-- Original >>>>>>>>>>>>>>>>>>>>>>>>>> -->
<div class="procat">
<img src="<?php echo get_template_directory_uri();?>/images/cat_original.png"/>
<div class="procatxt">
<h2>Necessidade de ter sempre a batata preferida.</h2>
<h3>A batata que todo mundo ama em quatro versões.</h3>
</div>
</div>
<div class="gridthumbs">
<?php echo do_shortcode('[summarize_posts post_type="produtos" taxonomy="prodline" taxonomy_term="Original Choice" order="DESC" orderby="menu_order"]<li><a href="[+permalink+]"><div class="gridthubox"><img src=[+prodimgprt:to_image_src:thumbnail+]></div><p class="prod">[+post_title+]</p></a></li>[/summarize_posts]') ?>
</div>


<!-- Signatures >>>>>>>>>>>>>>>>>>>>>>>>>> -->
<div class="procat">
<img src="<?php echo get_template_directory_uri();?>/images/cat_signatures.png"/>
<div class="procatxt">
<h2>Necessidade de se destacar.</h2>
<h3>Formatos únicos e inovadores para você incrementar de acordo com o seu restaurante.</h3>
</div></div>
<div class="gridthumbs">
<?php echo do_shortcode('[summarize_posts post_type="produtos" taxonomy="prodline" taxonomy_term="Menu Signatures" order="DESC" orderby="menu_order"]<li><a href="[+permalink+]"><div class="gridthubox"><img src=[+prodimgprt:to_image_src:thumbnail+]></div><p class="prod">[+post_title+]</p></a></li>[/summarize_posts]') ?>
</div>


<!-- Solutions >>>>>>>>>>>>>>>>>>>>>>>>>> -->
<div class="procat">
<img src="<?php echo get_template_directory_uri();?>/images/cat_solutions.png"/>
<div class="procatxt">
<h2>Necessidade de praticidade na operação.</h2>
<h3>Produtos feitos para um preparo mais fácil e uma exposição que dure mais.</h3>
</div></div>
<div class="gridthumbs">
<?php echo do_shortcode('[summarize_posts post_type="produtos" taxonomy="prodline" taxonomy_term="Chef Solutions" order="DESC" orderby="menu_order"]<li><a href="[+permalink+]"><div class="gridthubox"><img src=[+prodimgprt:to_image_src:thumbnail+]></div><p class="prod">[+post_title+]</p></a></li>[/summarize_posts]') ?>
</div>

			<?php get_sidebar('bottom'); ?>
             
<?php get_footer(); ?>