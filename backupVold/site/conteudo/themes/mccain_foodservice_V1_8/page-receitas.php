<?php
/*
Template Name: Receitas
*/
?>
<?php get_header(); ?>
			<?php get_sidebar('top'); ?>

			

<?php 
				if(have_posts()) {
					
					/* Start the Loop */ 
					while (have_posts()) {
						the_post();
						get_template_part('content', 'page');
						/* Display comments */
						if ( theme_get_option('theme_allow_comments')) {
							comments_template();
						}
					}

				} else {
				
					 theme_404_content();
					 
				} 
		    ?>
<!--
<?php
$Q = new GetPostsQuery();
$args = array();
$args['post_type'] = 'produtos';

$results = $Q->get_posts($args);




$select = "<select name='cat' id='cat' class='postform'>n";
$select.= "<option value='-1'>Escolha o tipo de estabelecimento</option>n";


foreach ($results as $r) {


         $select.= "<option value='".$r['guid']. "'>".$r['post_title']."</option>";
 }
  $select.= "</select>";
 
  echo $select;

?>


<script type="text/javascript">
    var dropdown = document.getElementById("cat");
    function onCatChange() {
        if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
            
$('#blmain').load (<?php print_r($r) ?>);       

}
    }
    dropdown.onchange = onCatChange;
</script>

<div id="blmain" class="bl_cont"></div>
-->

<div class="gridthumbs">
<?php echo do_shortcode('[summarize_posts post_type="receitas" order="DESC" orderby="post_date" ]<li><a href="[+permalink+]"><div class="gridthubox"><img src=[+thumbnail_src+]></div><p class="rec">[+post_title+]</p></a></li>[/summarize_posts]') ?>
</div>

			<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>