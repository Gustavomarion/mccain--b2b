<?php
/*
Template Name: Distribuidores
*/
?>
<?php get_header(); ?>
			<?php get_sidebar('top'); ?>

			

<?php 
				if(have_posts()) {
					
					/* Start the Loop */ 
					while (have_posts()) {
						the_post();
						get_template_part('content', 'page');
						
					}

				} else {
				
					 theme_404_content();
					 
				} 
		    ?>
<div class="bl_dist">
<div class="bl_map">

<?php echo do_shortcode("[show-map id='1']"); ?>

<h4>Quer se tornar um distribuidor McCain? Entre em contato com a gente. <a style="color: #000; text-decoration: none;" href="http://mccainfoodservice.com.br/contato/">Clique aqui</a></h4>

</div>

<div class="distside" >
<br>
<p>Selecione o estado:</p>
<br> 
<select id="dd_estados" name="dd_estados" 
onchange="if (value != '#'){
$('#distlist').load(value);
_gaq.push(['_trackPageview' , value ]);
}else{$('#distlist').unload()}">
<option value="#">- ESTADO -</option>

<option value="../estado/ac">Acre</option>
<option value="../estado/al">Alagoas</option>
<option value="../estado/ap">Amapá</option>
<option value="../estado/am">Amazonas</option>
<option value="../estado/ba">Bahia</option>
<option value="../estado/ce">Ceará</option>
<option value="../estado/df">Distrito Federal</option>
<option value="../estado/es">Espírito Santo</option>
<option value="../estado/go">Goiás</option>
<option value="../estado/ma">Maranhão</option>
<option value="../estado/mt">Mato Grosso</option>
<option value="../estado/ms">Mato Grosso do sul</option>
<option value="../estado/mg">Minas Gerais</option>
<option value="../estado/pa">Pará</option>
<option value="../estado/pb">Paraíba</option>
<option value="../estado/pr">Paraná</option>
<option value="../estado/pe">Pernambuco</option>
<option value="../estado/pi">Piauí</option>
<option value="../estado/rj">Rio de janeiro</option>
<option value="../estado/rn">Rio Grande do Norte</option>
<option value="../estado/rs">Rio Grande do sul</option>
<option value="../estado/ro">Rondônia</option>
<option value="../estado/rr">Roraima</option>
<option value="../estado/sc">Santa Catarina</option>
<option value="../estado/sp">São Paulo</option>
<option value="../estado/se">Sergipe</option>
<option value="../estado/to">Tocantins</option>






</select>

<div id="distlist"></div>
</div>
</div>
			<?php get_sidebar('bottom'); ?>
<?php get_footer(); ?>