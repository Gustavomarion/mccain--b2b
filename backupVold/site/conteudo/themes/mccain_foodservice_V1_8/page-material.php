<?php
/*
Template Name: Material 
*/
?>
<?php get_header(); ?>
<?php get_sidebar('top'); ?>
			<?php

			if (have_posts()) {
				/* Start the Loop */
				while (have_posts()) {
					the_post();
					get_template_part('content', 'page');
				}
			} else {
				theme_404_content();
			}
			?>

<div class="gridthumbs">
<?php echo do_shortcode('[summarize_posts post_type="material_promocional" ]<li><a href="[+matprom:get_post==guid+]"><div class="gridthubox"><img src=[+thumbnail_src:medium+]></div><p class="prod">[+post_title+]</p></a></li>[/summarize_posts]') ?>
</div>

			<?php get_sidebar('bottom'); ?>
             
<?php get_footer(); ?>