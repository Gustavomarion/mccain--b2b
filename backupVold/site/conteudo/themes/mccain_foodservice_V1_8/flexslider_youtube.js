jQuery(document).ready(function($) {
    $.getScript('//www.youtube.com/iframe_api');
    
    var cnt = 0;
    $('.flexslider iframe[src*=youtube]').each(function() {
        $(this).attr('id', 'youtubeplayer' + cnt);
        cnt++;
    });
    
    loadSlider();
    
    function loadSlider() {
        if(typeof(YT) != 'undefined') {
            $('.flexslider').flexslider({
                useCSS: false,
                animation: "slide",
                directionNav: true,
                controlsContainer: ".flex-container",
                animationLoop: true,
                pauseOnAction: true,
                pauseOnHover: true,
                video: true,
                after: function(slider) {
                    createPlayers(slider);
                },
                before: function() {
                    for (key in players) {
                        players[key].pauseVideo();
                    }
                },
                start: function(slider) {
                    createPlayers(slider);
                },
            });
            clearTimeout(timeout);
        } else {
            var timeout = setTimeout(loadSlider, 100);
        }    
    }
    
    var players = {};
    
    function createPlayers(slider) {
        var playtimer = [];
        (function($) {
            $(".flexslider iframe[src*=youtube]").each(function(index) {
                var frameID = this.id;
                if (frameID) { //If the frame exists
                    // we check if frame already has associated key in container object
                    if (!(frameID in players)) {
                        // now we check if the parent slider "row" is displayed
                        if ($(this).parents('.flex-active-slide').length > 0) {
                            // we create the player and add it to the container
                            players[frameID] = new YT.Player(frameID, {
                                events: {
                                    "onStateChange": function(event) {
                                        if(event.data == 1 || event.data == 3) {
                                            slider.flexslider('stop');
                                            for (var i=0; i<playtimer.length; i++) {
                                                clearTimeout(playtimer[i]);
                                            }
                                            playtimer = [];
                                        } else {
                                            // API sometimes sends multiple events when seeking
                                            playtimer.push(setTimeout(function(){
                                                slider.flexslider('play');
                                            }, 2000)); 
                                        }
                                    }
                                }
                            });
                        
                        }
                    }
                }
            });
        })(jQuery);
    }
}