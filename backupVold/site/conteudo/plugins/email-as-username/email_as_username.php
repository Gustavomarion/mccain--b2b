<?php
/*
Plugin Name: WP-Members Email as Username Extension
Plugin URI:  http://rocketgeek.com
Description: Sets WP-Members to use the email address as the username. 
Author:      Chad Butler
Version:     1.1
Author URI:  http://butlerblog.com
*/


/*  
	Copyright (c) 2006-2014  Chad Butler (email : plugins@butlerblog.com)

	The name WP-Members(tm) is a trademark of rocketgeek.com

	INSTALLATION INSTRUCTIONS
	
	This extension can be installed like any other plugin.  Place this file
	in your /wp-content/plugins/ folder, go the plugin admin panel and 
	activate WP-Members Email as Username Extension.
*/


/** Filters **/
add_filter( 'wpmem_inc_login_inputs', 'my_wpmem_remove_username_login_form' );
add_filter( 'wpmem_sidebar_form', 'my_wpmem_remove_username_sidebar_form' );
add_filter( 'wpmem_register_form_rows', 'my_wpmem_remove_username_register_form' );
add_filter( 'wpmem_pre_validate_form', 'my_wpmem_remove_username_pre_validate' ); 
add_filter( 'wpmem_msg_dialog', 'my_wpmem_remove_username_msg' );
add_filter( 'wpmem_inc_resetpassword_inputs', 'my_resetpwd_inputs' );
add_filter( 'wpmem_pwdreset_args', 'my_pwd_reset_args' );

/** Actions **/
add_action( 'wpmem_pre_update_data', 'my_wpmem_remove_username_update_email' );
add_action( 'wpmem_post_update_data', 'my_wpmem_remove_username_relogin' );


/** Begin functions **/


/**
 * Replace "Username" with "Email" in main login form
 */
function my_wpmem_remove_username_login_form( $rows ) { 
	foreach( $rows as $row => $sub ){
		if( strstr( $rows[$row]['name'], __( 'Username', 'wp-members' ) ) ){ 
			$rows[$row]['name'] = str_replace( __( 'Username', 'wp-members' ), __( 'Email', 'wp-members' ), $rows[$row]['name'] );
		}
	}
	return $rows;
}


/**
 * Replace "Username" with "Email" in  sidebar login form
 */
function my_wpmem_remove_username_sidebar_form( $form ) {
	return str_replace( '<label for="username">' . __( 'Username', 'wp-members' ), '<label for="username">' . __( 'Email', 'wp-members' ), $form );
}


/**
 * Remove "Username" from registration form
 */
function my_wpmem_remove_username_register_form( $rows ) { 
	foreach( $rows as $row => $sub ){
		if( strstr( $rows[$row]['label'], __( 'Username', 'wp-members' ) ) ){ 
			unset( $rows[$row] );
		}
	}
	return $rows;
}


/**
 * Set username field value as provided email during registration
 */
function my_wpmem_remove_username_pre_validate( $fields ) {
	$fields['username'] = $fields['user_email'];
	return $fields;
}


/**
 * Force username update in db when email is changed
 */
function my_wpmem_remove_username_update_email( $fields ) {	
	global $user_email, $wpdb, $wpmem_themsg;
	if( $fields['user_email'] != $user_email ) {
		/**
		 * email is being changed
		 * validate that email is not already used as a username
		 */
		if( username_exists( $fields['user_email'] ) ) {
			$dialogs = get_option( 'wpmembers_dialogs' );
			$wpmem_themsg = $dialogs[2];
			return $fields;
		} 
		
		// @todo - should check nicename/displayname to see if those are the same as the username
		
		/**
		 * update the username to the new email
		 */
		$wpdb->update( 
			$wpdb->users, 
			array( 'user_login' => $fields['user_email'] ), 
			array( 'ID' => $fields['ID'] ), 
			array( '%s' ) 
		);
	}
	return $fields;
}


/**
 * Relog the user in after update (new email = new username = invalid login)
 */
function my_wpmem_remove_username_relogin( $fields ) {
	wp_set_auth_cookie( $fields['ID'], '' );
	return $fields;
}


/**
 * Replace "username" with "email" in any dialog messages
 */
function my_wpmem_remove_username_msg( $str ) {
	return str_replace( 'username', 'email', $str ); 
}



/**
 * Set "Username" as "Email" in password reset form, remove other rows
 */
function my_resetpwd_inputs( $rows ) { 
	foreach( $rows as $row => $sub ){
		if( strstr( $rows[$row]['name'], __( 'Username', 'wp-members' ) ) ){ 
			$rows[$row]['name'] = 'Email';
		} else {
			unset( $rows[$row] );
		}
	}
	return $rows;
}


/**
 * Filter args for password reset to allow reset with just email address
 */
function my_pwd_reset_args( $args ) {
	
	if( isset( $_POST['user'] ) ) {
		$user_by = ( strpos( $_POST['user'], '@' ) ) ? 'email' : 'login';
		$user = get_user_by( $user_by, trim( $_POST['user'] ) );
	}
	
	if( $user_by == 'email' ) {
		return array( 'user' => $user->user_login, 'email' => $_POST['user'] );
	} else {
		return array( 'user' => $_POST['user'], 'email' => $user->user_email );
	}

	return $args;
}

/** END of FILE **/