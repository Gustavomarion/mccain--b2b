var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

//variables to check if width really changes
var width = jQuery(document).width();

 

jQuery(window).on('resize orientationchange', function() {
	

	if(jQuery(document).width() != width){

		width = jQuery(document).width();
	  
	  jQuery('#map_canvas').animate({
		    opacity: 0,
		  }, 100, function() {
		    // Animation complete.
		  });

		delay(function(){
		     if (typeof iwmDrawVisualization == 'function') {
				     iwmDrawVisualization();
				     console.log('Map Redraw Finished');
				     jQuery('#map_canvas').animate({
					    opacity: 1,
					  }, 100, function() {
					    // Animation complete.
					  });		 
				} 

		    }, 500);
	}

});