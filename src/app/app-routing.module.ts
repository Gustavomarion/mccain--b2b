
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NegocioComponent } from './pages/negocio/negocio.component';
import { CalculadoraComponent } from './pages/calculadora/calculadora.component';
import { DicasComponent } from './pages/dicas/dicas.component';
import { ReceitasComponent } from './pages/receitas/receitas.component';
import { BaixarComponent } from './pages/baixar/baixar.component';
import { MostraReceitaComponent } from './pages/receitas/mostra-receita/mostra-receita.component';
import { MostraProdutoComponent } from './pages/produtos/mostra-produto/mostra-produto.component';
import { ProdutosComponent } from './pages/produtos/produtos.component';
import { MostraDicaComponent } from './pages/dicas/mostra-dica/mostra-dica.component';
import { DistribuidoresComponent } from './pages/distribuidores/distribuidores.component';
import { MccainComponent } from './pages/mccain/mccain.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'negocio', component: NegocioComponent },
  { path: 'calculadora', component: CalculadoraComponent},

  { path: 'dicas', component: DicasComponent },
  { path: 'dicas/:id', component: MostraDicaComponent },

  { path: 'receitas', component: ReceitasComponent },
  { path: 'receitas/:id', component: MostraReceitaComponent },

  { path: 'produtos', component: ProdutosComponent },
  { path: 'produtos/:id', component: MostraProdutoComponent },

  { path: 'baixar', component: BaixarComponent },
  { path: 'distribuidores', component: DistribuidoresComponent },
  { path: 'mccain', loadChildren: './pages/mccain/mccain.module#MccainModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
