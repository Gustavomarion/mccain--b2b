import { HelperService } from './service/helper.service';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { CarrosselComponent } from './pages/home/carrossel/carrossel.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {NgxMaskModule, IConfig} from 'ngx-mask'
import { NgxCurrencyModule } from "ngx-currency";
export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

import { AppComponent } from './app.component';
import { NavComponent } from './layout/nav/nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NegocioComponent } from './pages/negocio/negocio.component';
import { CalculadoraComponent } from './pages/calculadora/calculadora.component';
import { DicasComponent } from './pages/dicas/dicas.component';
import { ReceitasComponent } from './pages/receitas/receitas.component';
import { BaixarComponent } from './pages/baixar/baixar.component';
import { MostraReceitaComponent } from './pages/receitas/mostra-receita/mostra-receita.component';
import { ProdutosComponent } from './pages/produtos/produtos.component';
import { MostraProdutoComponent } from './pages/produtos/mostra-produto/mostra-produto.component';
import { MostraDicaComponent } from './pages/dicas/mostra-dica/mostra-dica.component';

import { NavContentComponent } from './pages/produtos/nav-content/nav-content.component';
import { CmsService } from './service/cms.service';
import { DistribuidoresComponent } from './pages/distribuidores/distribuidores.component';
import { ProdutoService } from './service/produto.service';
import { AngularWebStorageModule } from 'angular-web-storage';

export const customCurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: ',',
  precision: 2,
  prefix: 'R$ ',
  suffix: '',
  thousands: '.',
  nullable: true
};


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    ContactComponent,
    NegocioComponent,
    CalculadoraComponent,
    DicasComponent,
    ReceitasComponent,
    BaixarComponent,
    MostraReceitaComponent,
    ProdutosComponent,
    MostraProdutoComponent,
    MostraDicaComponent,
    CarrosselComponent,
    NavContentComponent,
    DistribuidoresComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularWebStorageModule,
    LayoutModule,
    MatSidenavModule,
    SlickCarouselModule,
    MatIconModule,
    NgxMaskModule.forRoot(options),
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig)
  ],
  providers: [HelperService, CmsService, ProdutoService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
