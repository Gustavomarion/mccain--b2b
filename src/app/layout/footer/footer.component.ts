import { HelperService } from './../../service/helper.service';
import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-web-storage';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public policy: boolean;
  constructor(public helper: HelperService, public local: LocalStorageService) { }

  ngOnInit() {
    this.policy = this.local.get('policyAswner');
  }


  setPolicy() {
    this.local.set('policyAswner', true);
    this.policy = this.local.get('policyAswner');
  }

}
