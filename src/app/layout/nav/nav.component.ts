import { HelperService } from './../../service/helper.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProdutoService } from 'src/app/service/produto.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { CmsService } from 'src/app/service/cms.service';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

    public toggled = false;
    public search = false;
    public navTop: boolean;

    public searchCtrl: FormControl = new FormControl('');
    public produtos: any = [];
    public filteredProducts: Observable<any[]>;

    constructor(
        public helper: HelperService,
        private cmsService: CmsService
    ) { }

    ngOnInit() {
        this.getProdutos();
    }

    @HostListener('window:scroll', ['$event'])
    doSomething(event) {
        if (400 <= window.pageYOffset) {
            this.navTop = true;
        } else {
            this.navTop = false;
        }
    }

    toggleNav() {
        if (this.toggled) {
            this.toggled = false;
        } else {
            this.toggled = true;
        }
    }

    searcher() {
        this.search = true;
    }

    closeSearcher() {
        this.search = false;
        this.searchCtrl.setValue('');
    }



    async getProdutos(options: any = {}) {
        options.noPaginate = true;
        return await this.cmsService.pullItems('produtos').getAll(options).subscribe(async (response) => {
            this.produtos = await response;

            this.filteredProducts = this.searchCtrl.valueChanges.pipe(
                startWith(''),
                map((name: any) => name.trim() ? this._filter(name) : [])
            );
            return await response;
        });
    }

    public _filter(value: string) {
        const filterValue = value.toLowerCase();
        // return this.produtos.filter(option => option.toLowerCase().includes(filterValue));
        return this.produtos.filter((produto: any) => {
            if (produto.title.toLowerCase().includes(filterValue)
            || produto.subtitle.toLowerCase().includes(filterValue) ) {
                return produto;
            }
        });
    }

}
