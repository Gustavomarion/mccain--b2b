import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/service/produto.service';
import { CmsService } from 'src/app/service/cms.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, filter } from 'rxjs/operators';

@Component({
    selector: 'app-baixar',
    templateUrl: './baixar.component.html',
    styleUrls: ['./baixar.component.scss']
})
export class BaixarComponent implements OnInit {
    public produtos: any = [];
    public searchProduct = new FormControl();
    public categoryInput = new FormControl();

    public filteredProducts: Observable<any[]>;
    public categories: any = [];

    public produtoModal: any = {};
    public imageToDownload: any = { url: '', type: 1, embalagem: false };

    public loading: boolean;
    constructor(
        public cmsService: CmsService,
        private activatedRoute: ActivatedRoute
    ) { }
    ngOnInit() {
        this.loadCategories();

        this.getProdutos();

        this.categoryInput.valueChanges.subscribe((value) => {
            if (value !== '') {
                this.getProdutos({
                    filter: [
                        ['categories.name', value]
                    ]
                });
            } else {
                this.getProdutos();
            }
        });
    }

    async getProdutos(options: any = {}) {
        options.noPaginate = true;
        this.loading = true;
        return await this.cmsService.pullItems('produtos').getAll(options).subscribe(async (response) => {
            this.produtos = await response;

            this.filteredProducts = this.searchProduct.valueChanges.pipe(
                startWith(''),
                map(name => name.trim() ? this._filter(name) : this.produtos.slice())
            );
            this.searchProduct.markAsPending();
            setTimeout(() => {
                this.activatedRoute.params.subscribe((parans: any) => {
                    if (parans.produto && !options.filter) {
                        this.searchProduct.setValue(parans.produto);
                    }
                });
            }, 1000);
            this.loading = false;
            return await response;
        });
    }

    public loadCategories() {
        this.cmsService.pullItems('categories').getAll().subscribe((response) => {
            this.categories = response;
        });
    }

    public _filter(value: string) {
        const filterValue = value.toLowerCase();
        // return this.produtos.filter(option => option.toLowerCase().includes(filterValue));
        return this.produtos.filter((produto: any) => {
            if (produto.title.toLowerCase().includes(filterValue)) {
                return produto;
            }
        });
    }

    public download(url, name) {
        if ( this.imageToDownload.embalagem ) {
            const a = document.createElement('a');
            a.href = url;
            a.download = 'embalagem-' + name;
            a.click();
        } else {
            const ext = url.split('.');
            window.open(
                `${this.cmsService.defaultURL}/downloadImage?path=${url}&name=${name.replace(/ /g, '-')}&ext=.${ext[ext.length - 1]}`,
                '_blank' // <- This is what makes it open in a new window.
            );
        }
    }


    public selectProduct(produto) {
        this.produtoModal = produto;
        this.changeType(1);
    }

    public changeType(type: number, path?: string) {
        if (path) {
            console.log(path);
            this.imageToDownload.embalagem = true;
            this.imageToDownload.url =  path;
        } else {
            this.imageToDownload.embalagem = false;
            if (type === 1) {
                this.imageToDownload.url = this.produtoModal.images[0].url;
            } else {
                this.imageToDownload.url = this.produtoModal.thumbs[0].url;
            }
        }
        this.imageToDownload.type = type;
    }


    public fixPath(path) {
        path = path.replace(/[ÀÁÂÃÄÅ]/g, 'A');
        path = path.replace(/[àáâãäå]/g, 'a');
        path = path.replace(/[ÈÉÊË]/g, 'E');
        path = path.replace(/[èéêë]/g, 'E');
        path = path.replace(/[ÚÙÛÜ]/g, 'U');
        path = path.replace(/[úùûü]/g, 'u');
        path = path.replace(/[ÌÍÎÏ]/g, 'I');
        path = path.replace(/[)(}{]/g, '');
        return path.trim().replace(/ /g, '-');
    }
}
