import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
    public form = this.fb.group({
        email: [''],
        subject: [''],
        text: [''],
        cel: [''],
        name: ['', Validators.required],
        comments: ['', Validators.required],
        last_name: [''],
        restaurant_name: ['', Validators.required],
        restaurant_type: [''],
        street: ['', Validators.required],
        // state: [''],
        // city: ['', Validators.required],
        cep: [''],
        // country: ['']
    });
    public loading: boolean;
    public sended: boolean;

    @Input() public simpleForm = false;

    constructor(private fb: FormBuilder, private cms: CmsService) { }

    ngOnInit() {
    }


    private template(element) {
        return `
            <p>
                <strong>Comentários:</strong> ${element.comments} <br>
                <strong>Segmento de restaurante:</strong> ${element.restaurant_type}<br>
                <strong>Nome do restaurante:</strong> ${element.restaurant_name} <br>
                <strong>Endereço do restaurante:</strong> ${element.street} <br>
                <strong>CEP:</strong>${element.cep} <br>
            </p>
        `;

        // <strong>Estado:</strong>${element.state}<br>
        //         <strong>Cidade:</strong>${element.city}<br>
        //         <strong>Pais:</strong>${element.country } <br>

    }

    public send() {
        this.loading = true;
        this.cms.send(this.form.value).subscribe((response) => {
            // console.log(response)
            this.loading = false;
            this.sended = true;
        });
    }
}
