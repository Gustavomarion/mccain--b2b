import { CmsService } from './../../../service/cms.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-mostra-receita',
    templateUrl: './mostra-receita.component.html',
    styleUrls: ['./mostra-receita.component.scss']
})
export class MostraReceitaComponent implements OnInit {
    public receita: any;
    constructor(public cmsService: CmsService, private activeRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activeRoute.params.subscribe((parans: any) => {
            this.cmsService.pullItems('receitas').find(parans.id).subscribe((recipe: any) => {
                this.receita = recipe;
                this.receita.prepare_modes = JSON.parse(this.receita.prepare_modes);
            });
        });
    }



}
