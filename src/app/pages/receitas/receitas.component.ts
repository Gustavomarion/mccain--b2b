import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { CmsService } from 'src/app/service/cms.service';
import { ProdutoService } from 'src/app/service/produto.service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-receitas',
    templateUrl: './receitas.component.html',
    styleUrls: ['./receitas.component.scss']
})
export class ReceitasComponent implements OnInit {
    public productCtrl = new FormControl('');
    public receitas: any = [];

    constructor(
        public produtoService: ProdutoService,
        private cmsService: CmsService,
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.produtoService.loadProducts().then((response) => {
            this.productCtrl.valueChanges.subscribe(value => {
                if (value.trim()) {
                    this.loadReceitas({
                        filter: [
                            ['prepare_modes', 'like', `%25${value}%25`]
                        ]
                    });
                } else {
                    this.loadReceitas();
                }
            });
            this.activatedRoute.params.subscribe((parans: any) => {
                if (parans.produto) {
                    this.loadReceitas({
                        filter: [
                            ['prepare_modes', 'like', `%25${parans.produto}%25`]
                        ]
                    });

                    this.productCtrl.setValue(parans.produto);
                } else {
                    this.loadReceitas();
                }

            });

        });

    }



    loadReceitas(options: any = {}) {
        options.noPaginate = true;
        this.cmsService.pullItems('receitas').getAll(options).subscribe((receitas: any) => {
            this.receitas = receitas;
        });
    }


}
