import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-mostra-produto',
    templateUrl: './mostra-produto.component.html',
    styleUrls: ['./mostra-produto.component.scss']
})
export class MostraProdutoComponent implements OnInit {
    public produto: any = {} ;
    constructor(
        private cmsService: CmsService, 
        private activeRoute: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.activeRoute.params.subscribe((parans: any) => {
            // console.log(parans)
            this.cmsService.pullItems('produtos').find(parans.id).subscribe((response: any) => {
                this.produto = response;
                // console.log(response);
            });
        });

    }


    public download(path, name) {
        this.cmsService.download(path, name);
    }
    goTo(route: string) {
        this.router.navigate([route, { produto: this.produto.title }])
    }

}
