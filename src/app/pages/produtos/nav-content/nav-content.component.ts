import { HelperService } from './../../../service/helper.service';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { CmsService } from 'src/app/service/cms.service';
import { ProdutoService } from 'src/app/service/produto.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-nav-content',
    templateUrl: './nav-content.component.html',
    styleUrls: ['./nav-content.component.scss']
})
export class NavContentComponent implements OnInit, OnDestroy {
    @Input() public drawer: any;
    public items: any = [];

    constructor(
        private cmsService: CmsService,
        public produtoService: ProdutoService,
        public helper: HelperService,
        private activetedRoute: ActivatedRoute) { }

    ngOnInit() {
        if (this.helper.isMobile) {
            this.drawer.toggle();
        }
        // console.log(this.drawer.toggle())
        this.cmsService.pullItems('categories').getAll().subscribe((response) => {
            // console.log(response);
            this.activetedRoute.params.subscribe((params: any) => {
                response.map((category: any) => {
                    const obj: any = {
                        name: category.name,
                        toggled: false,
                        subItems: []
                    };

                    if (category.name.toString().includes(params.category) ) {
                        obj.toggled = true;
                    }
                    this.items.push(obj);
                });
            });

        }).add(() => {
            this.cmsService.pullItems('produtos/tags').customGet().subscribe((response: any) => {
                const obj: any = {
                    name: 'Tipo de restaurante',
                    toggled: false,
                    subItems: []
                };
                response.map((type: any) => {
                    obj.subItems.push({
                        name: type.name,
                        toggled: false,
                        tag_id: type.id
                    });
                });
                this.items.push(obj);

            });
        });


    }


    toggleCat(item?) {
        const idx = this.items.indexOf(item);
        if (this.items[idx].toggled) {
            this.items[idx].toggled = false;
            this.produtoService.currentCategory = '';
            this.produtoService.loadProducts();
        } else {
            this.deselectItems();
            if (this.items[idx].subItems.length < 1) {
                this.produtoService.loadProducts({
                    filter: [
                        ['categories.name', this.items[idx].name]
                    ]
                });
                this.produtoService.currentCategory = this.items[idx].name;
            }
            this.helper.scroll('nav');
            this.items[idx].toggled = true;
        }

        if ( this.helper.isMobile) {
            this.drawer.toggle();
        }
    }

    toggleSub(subitem: any, item: any) {
        const idx = this.items.indexOf(item);
        const idxSubItem = this.items[idx].subItems.indexOf(subitem);
        if (this.items[idx].subItems[idxSubItem].toggled) {
            this.items[idx].subItems[idxSubItem].toggled = false;
            this.items[idx].toggled = false;
            this.produtoService.currentCategory = '';
            this.produtoService.loadProducts();
        } else {
            this.deselectItems();
            this.cmsService.pullItems(`produtos/tags/${subitem.tag_id}`).customGet().subscribe((response: any) => {
                this.produtoService.produtos = response;
                this.items[idx].subItems[idxSubItem].toggled = true;
                this.items[idx].toggled = true;
                this.produtoService.currentCategory = subitem.name;
            });
        }

        // console.log(this.items[idx].subItems[idxSubItem].toggled)
    }
    public deselectItems(justSub = false) {
        this.items.map((item) => {
            if (!justSub) {
                item.toggled = false;
            }
            item.subItems.map((subitem: any) => {
                subitem.toggled = false;

            });
        });
    }

    ngOnDestroy() {
        this.produtoService.currentCategory = '';
    }

}
