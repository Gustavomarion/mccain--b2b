import { Component, OnInit, HostListener } from '@angular/core';
import { HelperService } from 'src/app/service/helper.service';
import { CmsService } from 'src/app/service/cms.service';
import { ProdutoService } from 'src/app/service/produto.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-produtos',
    templateUrl: './produtos.component.html',
    styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent implements OnInit {
    public items = [];
    public menuTop: boolean;
    constructor(
        public helper: HelperService,
        private cmsService: CmsService,
        public produtoService: ProdutoService,
        private activetedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.activetedRoute.params.subscribe((params: any) => {
            // console.log(params)
            if (params.category) {
                this.produtoService.loadProducts({
                    filter: [
                        ['categories.name', params.category]
                    ]
                }).then(() => {
                    this.produtoService.currentCategory = params.category;
                });
            } else {
                this.produtoService.loadProducts();
            }
        });
    }

    @HostListener('window:scroll', ['$event'])
    doSomething(event) {
        if (400 <= window.pageYOffset) {
            this.menuTop = true;
        } else {
            this.menuTop = false;
        }
    }

}
