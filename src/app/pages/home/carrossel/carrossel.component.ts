import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/service/helper.service';

@Component({
    selector: 'app-carrossel',
    templateUrl: './carrossel.component.html',
    styleUrls: ['./carrossel.component.scss']
})
export class CarrosselComponent implements OnInit {


    public slideConfig = {
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: true,
        arrows: false,
        responsive: [

        ]
    };

    public slides = [
        {
            title: `<br> <br>`,
            image: './assets/backgrounds/bg-cover-2-home.png',
            route: ['/produtos', {category: 'Para Delivery'}]
        },
        {
            title: `<br> <br>`,
            image: './assets/backgrounds/bg-cover-3-home.jpg',
            route: ['/negocio']
        },
        {
            title: `Porção para <span class="bold"> compartilhar </span> <br>
      é <span class="bold"> tudo de bom! </span> <br> Confira o portfólio
      ideal <br> para oferecer no <span class="bold"> happy <br> hour. </span>`,
            image: './assets/backgrounds/bg-cover-1-home.png',
            route: ['/produtos', {category: 'Para Compartilhar'}]
        }
    ];

    constructor(public helper: HelperService) { }

    ngOnInit() {
    }

    slickInit(e) {
    }
    breakpoint(e) {
    }
    afterChange(e) {
    }
    beforeChange(e) {
    }



}
