import { CmsService } from './../../service/cms.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-dicas',
    templateUrl: './dicas.component.html',
    styleUrls: ['./dicas.component.scss']
})
export class DicasComponent implements OnInit {
    public dicas: any = [];
    constructor(private cmsService: CmsService) { }

    ngOnInit() {
        this.loadDicas()
    }

    public loadDicas(options: any = {}) {
        options.noPaginate = true;
        this.cmsService.pullItems('blog/post').getAll(options).subscribe((response: any) => {
            console.log(response);
            this.dicas = response;
        });
    }
}
