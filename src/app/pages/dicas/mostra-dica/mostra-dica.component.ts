import { CmsService } from 'src/app/service/cms.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-mostra-dica',
    templateUrl: './mostra-dica.component.html',
    styleUrls: ['./mostra-dica.component.scss']
})
export class MostraDicaComponent implements OnInit {
    public dica: any;
    constructor(
        public cmsService: CmsService, 
        private activeRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.activeRoute.params.subscribe((parans: any) => {
            this.cmsService.pullItems('blog/post').find(parans.id).subscribe((dica: any) => {
                this.dica = dica;
            });
        });
    }
}
