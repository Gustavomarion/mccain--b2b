export const MapaData: any = [
    { uf: 'rr', name: 'Roraima', distributors: [] },
    { uf: 'ap', name: 'Amapá', distributors: [] },
    { uf: 'am', name: 'Amazonas', distributors: []},
    { uf: 'pa', name: 'Pará',  distributors: [
        {
            title: 'GUAJARA',
            action_locate: 'Porto Velhor e Ji- Paraná.',
            tel: '(69) 3421-2083',
            email: 'wagnercomercial@friocenter.com.br'
        },
        {
            title: 'Mercúrio  Alimentos',
            action_locate: 'Estado do Pará',
            tel: '91 3262-9500',
            email: 'comercial@mercurioalimentos.com.br'
        }
    ]},

    { uf: 'ac', name: 'Acre', distributors: [
        {
            title: 'FRIGOAVE',
            action_locate: 'Todo AC.',
            tel: '(68) 3223-0080',
            email: 'carlos@frigoaveac.com.br'
        },
        {
            title: 'FRIGOAVE',
            action_locate: 'Todo AC.',
            tel: '(68) 3223-0080',
            email: 'carlos@frigoaveac.com.br'
        },
        {
            title: 'FRIGOAVE',
            action_locate: 'Todo AC.',
            tel: '(68) 3223-0080',
            email: 'carlos@frigoaveac.com.br'
        },
    ]},
    { uf: 'ro', name: 'Rondônia', distributors: [
        {
            title: 'COIMBRA',
            action_locate: 'Todo estado de Rondônia, Acre e Sul Amazônia.',
            tel: '(69) 3216-2600',
            email: 'indisponível'
        },
        {
            title: 'GUAJARA',
            action_locate: 'Porto Velhor e Ji- Paraná.',
            tel: '(69) 3421-2083',
            email: 'wagnercomercial@friocenter.com.br'
        }
    ]},

    { uf: 'to', name: 'Tocantins', distributors: [] },
    { uf: 'ma', name: 'Maranhão', distributors: [
        {
            title: 'LDA',
            action_locate: 'São Luís.',
            tel: ' (98)  3878-4000 ',
            email: 'leandro@ldama.com.br'
        },
    ]},
    { uf: 'pi', name: 'Piauí', distributors: [
        {
            title: 'Frinor Alimentos ',
            action_locate: 'Teresina  e região metropolitana.',
            tel: '86 3221-1311 ',
            email: ' s.externocraveiro@gmail.com'
        }
    ]},

    { uf: 'ce', name: 'Ceará', distributors: [
        {
            title: 'J FRIOS  ',
            action_locate: 'Interior do Ceará (  Sobral e região ).',
            tel: ' (88) 3671-4040 ',
            email: ' jfcompras@hotmail.com '
        },
        {
            title: 'SOLMAR ',
            action_locate: 'estado do CE.',
            tel: '(85) 3131-1717 ',
            email: ' compras@solmar.com.br '
        }
    ]},

    { uf: 'rn', name: 'Rio Grande do Norte', distributors: [
        {
            title: 'CAICÓ FRIOS ',
            action_locate: 'Natal e região metropolitana ',
            tel: '84 3223-8772  ',
            email: ' contato@caicofrios.com.br  '
        }
    ]},
    { uf: 'pb', name: 'Paraíba', distributors: [
        {
            title: 'DJ distribuidora  ',
            action_locate: 'João Pessoa / Campina Grande ',
            tel: ' 83 3690-0231 ',
            email: 'comercial@voitafrios.com.br'
        },
        {
            title: 'LDA',
            action_locate: 'São Luís.',
            tel: ' (98)  3878-4000 ',
            email: 'leandro@ldama.com.br'
        },
        {
            title: 'DSG ',
            action_locate: 'Interior da Paraíba.',
            tel: '  83 35212697 ',
            email: 'comercial@voitafrios.com.br'
        },
        {
            title: 'ULTRA ',
            action_locate: 'João Pessoa / PB ',
            tel: '   83 99909-6395 ',
            email: 'contato@ultracd.com.br '
        }
    ]},

    { uf: 'pe', name: 'Pernambuco', distributors: [
        {
            title: ' J A CALHEIROS  ',
            action_locate: 'Estado de Pernambuco ',
            tel: '81 3471-2090 ',
            email: 'calheiros@manihot.com.br '
        },
        {
            title: ' Masterboi  ',
            action_locate: ' Estado de Pernambuco   ',
            tel: '0800-2813333 ',
            email: 'sac@masterboi.com.br '
        },
        {
            title: ' ASA BRANCA ',
            action_locate: 'Estado de Al ',
            tel: '(82) 3522-9300',
            email: 'ouvidoria@asabranca.ind.br '
        },
    ]},

    { uf: 'al', name: 'Alagoas', distributors: [
        {
            title: ' ASA BRANCA ',
            action_locate: 'Estado de Al ',
            tel: '(82) 3522-9300',
            email: 'ouvidoria@asabranca.ind.br '
        },
    ]},

    { uf: 'se', name: 'Sergipe', distributors: [
        {
            title: 'ASA BRANCA ',
            action_locate: 'Estado de Sergipe   ',
            tel: '79 3256-9104 ',
            email: ' ouvidoria@asabranca.ind.br '
        },
    ]},
    { uf: 'ba', name: 'Bahia', distributors: [
        {
            title: 'CANAA',
            action_locate: 'Estado da Bahia  ',
            tel: '(71) 3617-8900 ',
            email: 'contato@canaalimentos.com.br'
        },
        {
            title: 'MAIS COMERCIAL',
            action_locate: 'Sul da Bahia',
            tel: '(73) 2101-5877 ',
            email: ' atendimento@maislebon.com.br'
        },
    ]},

    { uf: 'mt', name: 'Mato Grosso', distributors: [
        {
            title: 'SE',
            action_locate: 'Todo MT.',
            tel: '(65) 3612-9900',
            email: 'televendas@sedistribuidora.com.br'
        },
    ]},

    { uf: 'go', name: 'Goiás',  distributors: [
        {
            title: 'DB Brasil',
            action_locate: 'Goiânia, Rio Verde, Anápolis, Jataí, Caldas Novas, Catalão, Inhumas e Cristalina.',
            tel: '(61) 3031-1400',
            email: 'marketing1@dbbrasil.com'
        },
        {
            title: 'JC',
            action_locate: 'Goiânia, Aparecida de Goiânia, Trindade, Senador Canedo e Caldas Novas.',
            tel: '(62) 4008-8500',
            email: 'comprasjc@jcdistribuicao.com.br'
        },
        {
            title: 'JC',
            action_locate: 'Goiânia',
            tel: '(62) 3240-9500',
            email: 'indisponível'
        },
    ]},

    { uf: 'ms', name: 'Mato Grosso do Sul', distributors: [
        {
            title: 'MANAH',
            action_locate: 'Campo Grande e Corumbá.',
            tel: '(67) 3388-9368',
            email: 'vidaalimentos@hotmail.com'
        },
    ]},

    { uf: 'mg', name: 'Minas Gerais', distributors: [] },
    { uf: 'es', name: 'Espírito Santo', distributors: [] },
    { uf: 'sp', name: 'São Paulo', distributors: [
        {
            title: 'Irmãos Avelino',
            action_locate: 'SP + Grande SP + Vale Paraíba',
            tel: '(11) 4146-7000 / (11) 4146-7070',
            email: 'vendas@iavelino.com.br'
        },
        {
            title: 'NetFeira (Demarchi)',
            action_locate: 'SP + Grande SP ',
            tel: '(11) 3643-8207 / (11) 3643-8210 / (11) 94298-7434',
            email: 'pedidos@demarchibrasil.com.br'
        },
        {
            title: 'Nelore',
            action_locate: 'Litoral Sul e Norte',
            tel: '(13) 3202-5151',
            email: 'compras@neloreprime.com.br'
        },
        {
            title: 'Mariusso',
            action_locate: 'Campinas, Jundiaí, Piracicaba, Araraquara, São Carlos, Limeira, Ribeirão Preto e região',
            tel: '(19) 3515-6500',
            email: 'gislaine.rodrigues@mariusso.com.br, alan.santos@mariusso.com.br '
        },
        {
            title: 'Riberfoods',
            action_locate: 'Ribeirão Preto, SJRP, Franca, Bauru, Jaú e região',
            tel: '(16) 99705-4919',
            email: 'compras1@rp.riberdoces.com.br'
        },
        {
            title: 'Speers Foods Imp. Exp. Com. de Produtos Alimenticios',
            action_locate: 'São Paulo Capital e Grande São Paulo',
            tel: '(11) 2523-7171',
            email: 'vendas1@speers.com.br'
        },
        {
            title: 'Bispo Alimentos',
            action_locate: 'São José do Rio Preto',
            tel: '(17) 99106-4503',
            email: 'danilosouza@bispoalimentos.com.br'
        },
    ]},

    { uf: 'rj', name: 'Rio de Janeiro', distributors: [
        {
            title: 'TOP ALTO ALIMENTOS',
            action_locate: 'RIO E GRANDE RIO.',
            tel: '(21) 2757-5500',
            email: 'contato@topalto.com.br'
        },
    ]},

    { uf: 'pr', name: 'Paraná',  distributors: [
        {
            title: 'OESA COMÉRCIO E REPRESENTAÇÕES LTDA.',
            action_locate: 'Todo estado de Santa Catarina/Curitiba e região metropolitana/Litoral PR e Centro-Sul Paranaense',
            tel: '(47) 3376-9500',
            email: 'comercial@oesa.com.br/sac@oesa.com.br'
        },
        {
            title: 'DOMINGUES & KESSA LTDA',
            action_locate: 'Região Norte do Paraná',
            tel: '(44) 3027-5151',
            email: 'rodrigo@multifrios.com'
        },
        {
            title: 'DANJOS COMERCIAL GÊNEROS ALIMS. LTDA',
            action_locate: 'Região Norte do Paraná',
            tel: '(43) 3276-2788',
            email: 'danjos.vendas@sercomtel.com.br'
        },
        {
            title: 'POLINA ALIMENTOS',
            action_locate: 'Região Oeste do Paraná',
            tel: '(45) 3324-0101',
            email: 'polina@polinaalimentos.com.br'
        },
        {
            title: 'DISTRIBUIDORA DE FRIOS ALVORADA',
            action_locate: 'Região Oeste do Paraná',
            tel: '(45) 3522-1511',
            email: 'contato@difaldistribuidora.com '
        },
    ]},

    { uf: 'sc', name: 'Santa Catarina', distributors: [
        {
            title: 'OESA COMÉRCIO E REPRESENTAÇÕES LTDA.',
            action_locate: 'Todo estado de Santa Catarina/Curitiba e região metropolitana/Litoral PR e Centro-Sul Paranaense',
            tel: '(47) 3376-9500',
            email: 'comercial@oesa.com.br/sac@oesa.com.br'
        },
    ]},

    { uf: 'rs', name: 'Rio Grande do Sul', distributors: [
        {
            title: 'PERTE DISTRIBUIDORA DE ALIMENTOS LTDA.',
            action_locate: 'Estado do Rio Grande do Sul',
            tel: '(51) 3441-6700',
            email: 'perte@perte.com.br'
        },
        {
            title: 'JOHANN ALIMENTOS',
            action_locate: 'Porto Alegre, Grande Porto Alegre, litoral, região carbonífera e Serra',
            tel: '(51) 3561-4500',
            email: 'johann@johann.com.br'
        },
        {
          title: 'PMI – Food Service',
          action_locate: 'Porto Alegre, Grande Porto Alegre',
          tel: '(51) 3115-8001',
          email: 'info@pmifoodservice.com.br'
      }
    ]},
];
