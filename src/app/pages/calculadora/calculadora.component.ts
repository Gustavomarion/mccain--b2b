import { HelperService } from './../../service/helper.service';
import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/service/produto.service';
import { FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'app-calculadora',
    templateUrl: './calculadora.component.html',
    styleUrls: ['./calculadora.component.scss']
})
export class CalculadoraComponent implements OnInit {

    public form = this.fb.group({
        size: [''],
        cost_price: [''],
        sale_price: [''],
        cost_per: [0],
        profit_per: [0],
        markup: [0]
    });

    public productCtrl = new FormControl('', Validators.required);
    public perUnit: boolean;
    constructor(
        public produtosService: ProdutoService,
        private fb: FormBuilder,
        public helper: HelperService
    ) { }

    ngOnInit() {
        this.produtosService.loadProducts();
        this.productCtrl.valueChanges.subscribe((id: number) => this.indentifyProduct(id));
        console.log();
    }


    get markup() {
        return this.form.controls.markup.value;
    }

    get profit_per() {
        return this.form.controls.profit_per.value;
    }

    get cost_per() {
        return this.form.controls.cost_per.value;
    }

    public indentifyProduct(id) {
        if ([23, 15, 19].find((i) => i === parseInt(id, 10))) {
            this.perUnit = true;
        } else{
            this.perUnit = false;
        }
        this.form.reset({
            cost_per: 0,
            cost_price: 0,
            markup: 0,
            size: '',
        });
    }

    public calc() {
        // console.log(this.form.value.size, 'tamanho');
        // console.log(this.form.value.cost_price, 'preco de custo');
        // console.log(this.form.value.sale_price, 'preco de venda');
        if (this.perUnit) {
            this.form.controls.cost_per.setValue(
                (parseFloat(this.form.value.size) * 0.02) * parseFloat(this.form.value.cost_price)
            );
        } else {
            this.form.controls.cost_per.setValue(
                (parseFloat(this.form.value.size) * 0.001) * parseFloat(this.form.value.cost_price)
            );
        }

        this.form.controls.profit_per.setValue(
            (parseFloat(this.form.value.sale_price) - this.form.value.cost_price) * this.form.value.size
        );
        this.form.controls.markup.setValue(
            this.profit_per / this.cost_per
        );
    }

}
