import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MccainComponent } from './mccain.component';
import { CarouselHistoryComponent } from './carousel-history/carousel-history.component';
import { CarouselCuriosidadeComponent } from './carousel-curiosidade/carousel-curiosidade.component';
import { CarouselMissionComponent } from './carousel-mission/carousel-mission.component';

const routes: Routes = [
    {
        path: '',
        component: MccainComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'historia' },
            { path: 'historia', component: CarouselHistoryComponent },
            { path: 'missao', component: CarouselMissionComponent },
            { path: 'curiosidade', component: CarouselCuriosidadeComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MccainRoutingModule { }
