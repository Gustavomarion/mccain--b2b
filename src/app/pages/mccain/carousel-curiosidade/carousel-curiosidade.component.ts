import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/service/helper.service';

@Component({
    selector: 'app-carousel-curiosidade',
    templateUrl: './carousel-curiosidade.component.html',
    styleUrls: ['./carousel-curiosidade.component.scss', '../mccain.component.scss']
})
export class CarouselCuriosidadeComponent implements OnInit {
    public slideConfig = {
        autoplay: true,
        autoplaySpeed: 1000,
        arrows: false,
        centerMode: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        infinite: false,
        responsive: [
            {
                breakpoint: 1242,
                settings: {
                    centerMode: false,
                    centerPadding: '0px',
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: `60px`,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    centerPadding: `70px`,
                    slidesToShow: 1
                }
            }

        ]
    };


    public slides = [
        {
            type: 2,
            line: ``,
            line2: ``
        },
        {
            type: 0,
            line: `Sabia que o congelamento é o mais eficiente sistema de preservação de alimentos?`,
            line2: `Nos Estados Unidos, a batata frita é conhecida como french fries, ou seja, batatas francesas.`
        },

        {
            type: 1,
            line: `Quem inventou a batata frita? Os belgas insistem que foram eles,
                    apesar de algumas histórias falaram de uma origem norte-americana.`,
            line2: `Batata frita também é cultura. Em Bruxelas, existe um museu dedicado à batata frita, o Fritemuseum.`
        },

        {
            type: 0,
            line: `Pode viajar tranquilo. Hoje, você encontra as batatas McCain em cerca de 160 países.`,
            line2: `A batata é um super-alimento. É adaptável aos mais diversos climas, rica em nutrientes e vitaminas dos complexos B e C.`
        },
        {
            type: 0,
            line: ` Uma lenda conta que quando os invernos eram rígidos e ficava impossível pescar,
                    os belgas fritavam batata frita para enganar a vontade de comer peixe.`,
            line2: `Com catchup, maionese e mostarda você já ouviu falar.
                     Mas sabe que tem gente que adora fritas com sorvete?`
        }
    ];
    constructor(public helper: HelperService) { }

    ngOnInit() {
        const length = this.slides.length;
        for (let i = 0; i < length; i++) {
            this.slides.push(this.slides[i]);
        }
    }


    slickInit(e) {
    }
    breakpoint(e) {
    }
    afterChange(e) {
    }
    beforeChange(e) {
    }
}
