import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/service/helper.service';

@Component({
    selector: 'app-carousel-history',
    templateUrl: './carousel-history.component.html',
    styleUrls: ['./carousel-history.component.scss', '../mccain.component.scss']
})
export class CarouselHistoryComponent implements OnInit {
    public slideConfig = {
        autoplay: true,
        autoplaySpeed: 1000,
        arrows: false,
        centerMode: false,
        centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,

        responsive: [{
            breakpoint: 768,
            settings: {
                infinite: false,
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        }]
    };

    public slides = [
        {
            img: './assets/mccain/historia-1909-familia-mccain@2x.jpg',
            year: '1909',
            info: `No Canadá, a família McCain começa a comercializar batatas-sementes e batatas in natura.`
        },
        {
            img: './assets/mccain/historia-1914-irmaos-mccain@2x.jpg',
            year: '1914',
            info: `A empresa McCain é oficialmente fundada pelos irmãos Harrison e Wallace McCain.`
        },
        {
            img: './assets/mccain/historia-1957-abertura-fabrica-mccain@2x.jpg',
            year: '1957',
            info: `Abertura da primeira fábrica de batatas pré-fritas congeladas, em Florenceville, Canadá. Nos
                        anos seguintes, o crescimento acelerado leva a McCain para outros continentes.`
        },
        {
            img: './assets/mccain/historia-1922-mccain-brasil@2x.jpg',
            year: '1992',
            info: `Chegada ao Brasil, com um escritório comercial em São Paulo.
                      Os produtos eram importados das fábricas do Canadá e da Europa.`
        },
        {
            img: './assets/mccain/ano-1995.jpg',
            year: '1995',
            info: `Inauguração da fábrica na Argentina.
            Localizada em Balcarce, a maior e mais moderna fábrica da América Latina nasceu para atender todo o mercado sul-americano.`
        },
        {
            img: './assets/mccain/ano-2000.jpg',
            year: '2000',
            info: `Expansão na fábrica Argentina, com um investimento de US$ 80 milhões.`
        },
        {
            img: './assets/mccain/ano-2003.jpg',
            year: '2003',
            info: `Abertura da filial do escritório comercial no Rio de Janeiro.`
        },
        {
            img: './assets/mccain/ano-2016.jpg',
            year: '2016',
            info: `Abertura Centro de Distribuição em Pernambuco.`
        },
        {
            img: './assets/mccain/ano-2017.jpg',
            year: '2017',
            info: `Abertura Centro de Distribuição em Santa Catarina.`
        },
        {
            img: './assets/mccain/ano-2018.jpg',
            year: '2018',
            info: `McCain adquire 49% da empresa Forno de Minas.`
        },
        {
            img: './assets/mccain/ano-2019.jpg',
            year: '2019',
            info: `McCain adquire 70% da empresa Serya.`
        },
        {
            img: './assets/mccain/ano-hoje.jpg',
            year: 'Hoje',
            info: `Hoje a McCain conta com 51 fábricas de alimentos congelados, mais de 20 mil colaboradores e um
            faturamento mundial de 7 bilhões de dólares.
            A empresa é líder mundial em processamento e vendas de batatas pré-fritas congeladas,
            criando milhares de empregos e alimentando famílias ao redor do mundo.`
        }
    ];
    constructor(public helper: HelperService) { }

    ngOnInit() {
        // const qtd = this.slides.length ;
        // for (let i = 0; i < qtd; i++) {
        //     this.slides.push(this.slides[i]);
        // }
    }

    slickInit(e) {
        // console.log(e);
    }
    breakpoint(e) {
    }
    afterChange(e) {
    }
    beforeChange(e) {
    }
}
