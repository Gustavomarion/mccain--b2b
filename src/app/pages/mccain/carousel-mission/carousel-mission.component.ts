import { HelperService } from './../../../service/helper.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-carousel-mission',
    templateUrl: './carousel-mission.component.html',
    styleUrls: ['./carousel-mission.component.scss', '../mccain.component.scss']
})
export class CarouselMissionComponent implements OnInit {
    public slideConfig = {
        autoplay: true,
        autoplaySpeed: 4000,
        arrows: false,
        slidesToShow: 1,
        infinite: false
    };

    public slides = [
        {
            type: 0,
            img : './assets/mccain/missao-batatas-variedade-mccain@2x.jpg',
            imgAlt : 'Variedade de Batatas McCain',
            text : `
                <ul class="m-o-v " >
                    <li>
                        <h3 class="text-koho-bold">Nossa missão</h3>
                        <p class="text-koho">
                            Entregar qualidade e soluções inovadoras em alimentação para garantir o crescimento lucrativo e 
                            constante dos negócios, além da satisfação e do bem-estar dos clientes, 
                            consumidores, colaboradores e da comunidade.
                        </p>
                    </li>
                    <li>
                        <h3 class="text-koho">Nosso objetivo</h3>
                        <p class="text-koho">
                            Ser a marca preferida em alimentação, adorada pelas pessoas, e contribuir para o bem-estar de todos.
                        </p>
                    </li>

                    <li>
                        <h3 class="text-koho">Nossos valores</h3>
                        <ul>
                            <li class="text-koho">
                            Trabalhamos todos os dias para colocar um SORRISO no rosto de nossos clientes e consumidores.</li>
                            <li class="text-koho">
                            Valorizamos a MELHORIA CONTÍNUA de nossos colaboradores, produtos e negócios.</li>
                            <li class="text-koho">
                            Encorajamos a INICIATIVA e a INOVAÇÃO: tentar e falhar é aceitável; falhar por não tentar não. </li>
                            <li class="text-koho">Ousamos ser DIFERENTES: perseguimos novas ideias, buscamos inovação e diferenciação. </li>
                            <li class="text-koho">Valorizamos o TRABALHO EM EQUIPE e a troca de ideias. </li>
                            <li class="text-koho">
                            CONQUISTAMOS mercado a mercado e criamos vantagens competitivas elevando a nossa ESCALA GLOBAL.  </li>
                            <li class="text-koho">
                            Acreditamos que integridade, honestidade e ética são parte do nosso sucesso. BOA ÉTICA é bom negócio. </li>
                            <li class="text-koho">
                            Somos MULTICULTURAIS e nos preocupamos com nossa equipe, com as famílias e com a comunidade local.</li>
                            <li class="text-koho">Temos orgulho em ser uma empresa FAMILIAR. </li>
                        </ul>
                    </li>
                    <li >
                        <p class="text-koho">
                            Para garantir que nossas batatas naturalmente gostosas
                            cheguem até você com a qualidade ideal, a McCain segue
                            valores e objetivos com seriedade e muito comprometimento.
                        </p>
                    </li>
                </ul>
           `
        }
    ];

    constructor(public helper: HelperService) { }

    ngOnInit() {
        if (this.helper.isMobile) {
            this.slides.splice(1, 1);
        }
    }

    slickInit(e) {
    }
    breakpoint(e) {
    }
    afterChange(e) {
    }
    beforeChange(e) {
    }
}
