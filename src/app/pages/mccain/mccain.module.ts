import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CarrosselComponent } from './../home/carrossel/carrossel.component';
import { MccainComponent } from './mccain.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { MccainRoutingModule } from './mccain-routing.module';
import { CarouselHistoryComponent } from './carousel-history/carousel-history.component';
import { CarouselCuriosidadeComponent } from './carousel-curiosidade/carousel-curiosidade.component';
import { CarouselMissionComponent } from './carousel-mission/carousel-mission.component';

@NgModule({
  declarations: [
    MccainComponent,
    CarouselHistoryComponent,
    CarouselCuriosidadeComponent,
    CarouselMissionComponent
  ],
  imports: [
    CommonModule,
    MccainRoutingModule,
    SlickCarouselModule
  ]
})
export class MccainModule { }
