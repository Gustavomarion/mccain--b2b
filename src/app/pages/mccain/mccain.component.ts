import { HelperService } from './../../service/helper.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mccain',
  templateUrl: './mccain.component.html',
  styleUrls: ['./mccain.component.scss']
})
export class MccainComponent implements OnInit {

  constructor(public helper: HelperService) { }

  ngOnInit() {
  }

}
