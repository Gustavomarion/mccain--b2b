import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'mccain-b2b';
    constructor(router: Router) {
        const navEndEvents = router.events.pipe(
            filter(event => event instanceof NavigationEnd),
        );

        navEndEvents.subscribe((event: NavigationEnd) => {
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            })
        });

    }
}
