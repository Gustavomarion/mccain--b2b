import { Injectable } from '@angular/core';
import { CmsService } from './cms.service';

@Injectable({
    providedIn: 'root'
})
export class ProdutoService {
    public currentCategory = '';
    public produtos: any = [];

    constructor(private cmsService: CmsService) { }

    public selectCategory(categoryName: string) {
        this.currentCategory = categoryName;
    }

   async loadProducts(options: any = {}) {
        options.noPaginate = true;
        this.produtos = [];
        return await this.cmsService.pullItems('produtos').getAll(options).subscribe( async (response) => {
            this.produtos = await response.sort((a, b) => {
                if (a.title > b.title) {
                  return 1;
                }
                if (a.title < b.title) {
                  return -1;
                }
                // a must be equal to b
                return 0;
            });
        });
    }
}
